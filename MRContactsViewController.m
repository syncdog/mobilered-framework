//
//  MRContactsViewController.m
//  MobileRed
//
//  Created by Lion User on 23/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRContactsViewController.h"
#import "MRContact.h"
#import "MRAddressBook.h"
#import "MRContactDetailsViewController.h"

@implementation MRContactsViewController

@synthesize contactsArray = _contactsArray;
@synthesize sectionNames = _sectionNames;
@synthesize theContact = _theContact;
@synthesize isCheckable = _isCheckable;

-(id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
   
    return self;
}

-(id)initWithStyle:(UITableViewStyle)style isCheckable:(BOOL)checkable {
    self = [super initWithStyle:style];
    
    if (self) {
        self.isCheckable = checkable;
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc {
    [_contactsArray release];
    [_sectionNames release];
    
    if (_theContact != nil) {
        [_theContact release];
    }
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Contacts", nil);
    
    self.contactsArray = [((MRAddressBook *)[MRRuntimeStoreAccessor getFromRuntimeStore:kAddressBook]) getContacts];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_contactsArray != nil) {
        MRAddressBook *book = [MRRuntimeStoreAccessor getFromRuntimeStore:kAddressBook];
        switch (section) {
            case 0:
                return [book.groupContacts count];
                break;
            case 1:
                return [book.individualContacts count];
                break;
            default:
                break;
        }
    }
    
    return 0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return NSLocalizedString(@"Groups", nil);
            break;
        case 1:
            return NSLocalizedString(@"Contacts", nil);
            break;
        default:
            break;
    }
    
    return nil;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    NSArray *names = [[NSArray alloc] initWithObjects:NSLocalizedString(@"Groups", nil),NSLocalizedString(@"Contacts", nil), nil];
    self.sectionNames = names;
    
    [names release];
    
    return _sectionNames;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    NSInteger row = indexPath.row;
    MRContact *contact = [_contactsArray objectAtIndex:row];
    NSString *title = contact.name;
    cell.textLabel.text = title;
    
    if (contact.cecked) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isCheckable) {
        MRContact *ctc = [_contactsArray objectAtIndex:indexPath.row];
        ctc.cecked = TRUE;
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    } else {
        self.theContact = [_contactsArray objectAtIndex:indexPath.row];
        
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:NSLocalizedString(@"Details", nil) otherButtonTitles:NSLocalizedString(@"Send MRED message", nil), NSLocalizedString(@"Call", nil), NSLocalizedString(@"Send Email", nil), nil];
        
        [sheet showInView:self.view];
        
        [sheet release];
    } 
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UI sheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    UIViewController *controller = nil;
    
    switch (buttonIndex) {
        case 0:
            controller = [[MRContactDetailsViewController alloc] initWithContact:_theContact];
            break;
        case 1:
            
            break;
        case 2:
            
            break;
        case 3:
            
            break;
        default:
            break;
    }
    
    [self.navigationController pushViewController:controller animated:YES];
    
    [controller release];
}

@end
