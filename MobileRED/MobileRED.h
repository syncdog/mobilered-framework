//
//  MobileRED.h
//  MobileRED
//
//  Created by Dima Yarmoshuk on 9/20/15.
//  Copyright (c) 2015 SyncDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RuntimePersistenceHelper.h"
#import "GMMainMenuViewController.h"
#import <UIKit/UIKit.h>
#import "MRWebViewController.h"


@interface MobileRED : NSObject

@property (nonatomic, retain) UINavigationController *navController;
@property (nonatomic, retain) RuntimePersistenceHelper *helper;
@property (nonatomic, retain) NSMutableDictionary *runtimeStore;

+ (instancetype)sharedManager;
- (GMMainMenuViewController *)mobileREDMainViewController;
- (MRWebViewController *)mobileREDWebViewController;
- (void)applicationDidEnterBackground;
- (void)applicationWillEnterForeground;
- (void)applicationDidBecomeActive;
- (void)applicationWillTerminate;

@end
