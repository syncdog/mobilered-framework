//
//  MobileRED.m
//  MobileRED
//
//  Created by Dima Yarmoshuk on 9/20/15.
//  Copyright (c) 2015 SyncDog. All rights reserved.
//

//
//  MobileRED.m
//  MobileRED
//
//  Created by Dima Yarmoshuk on 9/20/15.
//
//

#import "MobileRED.h"
//#import "AFNetworking.h"

@implementation MobileRED

static MobileRED *_sharedManager = nil;

+(instancetype)sharedManager {
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        if (_sharedManager == nil)
            _sharedManager = [[MobileRED alloc] init];
    });
    
    return _sharedManager;
}

- (GMMainMenuViewController *)mobileREDMainViewController {
    NSBundle *resources = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]];
    if (resources) {
        GMMainMenuViewController *vc = [[GMMainMenuViewController alloc] initWithNibName:@"MainMenu" bundle:resources];
        if (vc != nil)
            return vc;
        else {
            NSLog(@"Cant get main controller xib");
            return nil;
        }
    }
    else {
        NSLog(@"No bundle resource found");
        return nil;
    }
    
}

- (MRWebViewController *)mobileREDWebViewController {
    NSBundle *resources = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]];
    if (resources) {
        MRWebViewController *vc = [[MRWebViewController alloc] initWithNibName:@"MRWebViewController" bundle:resources];
        if (vc != nil)
            return vc;
        else {
            NSLog(@"Cant get main controller xib");
            return nil;
        }
    }
    else {
        NSLog(@"No bundle resource found");
        return nil;
    }

}

- (void)applicationDidEnterBackground {
    
    [_sharedManager.helper writeToFile:_sharedManager.runtimeStore];
    
    _sharedManager.runtimeStore = nil;
}
- (void)applicationWillEnterForeground {
    
    _sharedManager.runtimeStore = _sharedManager.helper.dictionary;
    
}
- (void)applicationDidBecomeActive {

    _sharedManager.runtimeStore = _sharedManager.helper.dictionary;
    
}
- (void)applicationWillTerminate {
    
    [_sharedManager.helper writeToFile:_sharedManager.runtimeStore];
    
    _sharedManager.runtimeStore = nil;
}


@end
