//
//  MRFormsXMLRequester.h
//  MobileRed
//
//  Created by Lion User on 10/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequester.h"

@interface MRFormsXMLRequester : MRHttpRequester

-(void)getFormXML:(NSString *)formId;

@end
