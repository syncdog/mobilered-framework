//
//  MRMessageTypeViewController.h
//  MobileRed
//
//  Created by Lion User on 26/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kPushButtonTag 0
#define kSmsButtonTag 1
#define kAnyButtonTag 2

@interface MRMessageTypeViewController : UIViewController {
    UIButton *_pushButton, *_smsButton, *_anyButton;
}

@property (nonatomic, retain) IBOutlet UIButton *pushButton, *smsButton, *anyButton;

-(IBAction)buttonPressed:(id)sender;

@end
