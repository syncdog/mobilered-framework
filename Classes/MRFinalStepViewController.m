//
//  MRFinalStepViewController.m
//  MobileRed
//
//  Created by Marius Gherman on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRFinalStepViewController.h"
#import "MRMREDMessage.h"
#import "MRTemplate.h"
#import "MRMessageSender.h"

@implementation MRFinalStepViewController

@synthesize message = _message;

@synthesize subjectLabel = _subjectLabel;
@synthesize messageLabel = _messageLabel;
@synthesize subjectText = _subjectText;
@synthesize messageText = _messageText;

-(id)initWithMessage:(MRMREDMessage *)theMessage {
    self = [super initWithNibName:@"MRFinalStepViewController" bundle:[NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]]];
    
    if (self) {
        self.message = theMessage;
    }
    
    return self;
}

-(void)dealloc {
    [_message release];
    
    [_subjectLabel release];
    [_subjectText release];
    [_messageLabel release];
    [_messageText release];
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"Finish - Step 2", nil);
    
    _subjectLabel.text = NSLocalizedString(@"Subject :", nil);
    _subjectText.placeholder = NSLocalizedString(@"<Message subject>", nil);
    
    _messageLabel.text = NSLocalizedString(@"Message body :", nil);
    
    if (_message.subject != nil || _message.message != nil) {
        if(_message.subject != nil) {
            _subjectText.text = _message.subject;
        }
        
        if (_message.message != nil) {
            _messageText.text = _message.message;
        }
    } else if (_message.templateT != nil) {
        _subjectText.text = _message.templateT.title;
        _messageText.text = _message.templateT.message;
    }
    
    UIBarButtonItem *item =[[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Send", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(nextStep)] autorelease];
    
    [self.navigationItem setRightBarButtonItem:item animated:YES];
    
    UIButton *backButton = [UIButton buttonWithType:101];
    [backButton addTarget:self action:@selector(backStep) forControlEvents:UIControlEventTouchUpInside];
    [backButton setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateNormal];
    
    UIBarButtonItem *item1 = [[[UIBarButtonItem alloc] initWithCustomView:backButton] autorelease];
    
    [self.navigationItem setLeftBarButtonItem:item1 animated:YES];
}

-(void)nextStep {
    MRMessageSender *sender = [[[MRMessageSender alloc] initWithMessage:_message] autorelease];
    [sender sendMessage];
}

-(void)backStep {
    _message.subject = _subjectText.text;
    _message.message = _messageText.text;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.subjectLabel = nil;
    self.subjectText = nil;
    self.messageLabel = nil;
    self.messageText = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField; {
    return [textField resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    textView.textColor = [UIColor darkGrayColor];
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

@end
