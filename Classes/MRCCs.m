//
//  MRCCs.m
//  MobileRed
//
//  Created by Lion User on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRCCs.h"

@implementation MRCCs

@synthesize idCC = _idCC;
@synthesize name = _name;
@synthesize number = _number;
@synthesize otherNumber = _otherNumber;

-(id)initWithString:(NSString *)ccString {
    self = [super init];
    
    if (self) {
        NSArray *splited = [ccString componentsSeparatedByString:@"|"];
        
        self.idCC = [splited objectAtIndex:1];
        self.name = [splited objectAtIndex:2];
        self.number = [splited objectAtIndex:3];
        self.otherNumber = [splited objectAtIndex:4];
    }
    
    return  self;
}


#pragma NSCoding methods 

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if (self) {
        self.idCC = [aDecoder decodeObjectForKey:kIdCCKey];
        self.name = [aDecoder decodeObjectForKey:kNameKey];
        self.number = [aDecoder decodeObjectForKey:kNumberKey];
        self.otherNumber = [aDecoder decodeObjectForKey:kOtherNumberKey];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_idCC forKey:kIdCCKey];
    [aCoder encodeObject:_name forKey:kNameKey];
    [aCoder encodeObject:_number forKey:kNumberKey];
    [aCoder encodeObject:_otherNumber forKey:kOtherNumberKey];
}

@end
