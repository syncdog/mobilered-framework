//
//  MRMessageLevelViewController.h
//  MobileRed
//
//  Created by Lion User on 26/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kEmergencyButtonTag 0
#define kWarningButtonTag 1
#define kInformationButtonTag 2

@class MRMREDMessage;

@interface MRMessageLevelViewController : UIViewController {
    MRMREDMessage *_message;
    UIButton *_emergencyButton, *_warningButton, *_informationButton;
}

@property (nonatomic, retain) MRMREDMessage *message;
@property (nonatomic, retain) IBOutlet UIButton *emergencyButton, *warningButton, *informationButton;

-(id)initWithMessageType:(MRMREDMessage *)theMessage;
-(IBAction)buttonPressed:(id)sender;

@end
