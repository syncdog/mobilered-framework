//
//  MRFileDownloader.h
//  MobileRed
//
//  Created by Lion User on 19/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"

@interface MRFileDownloader : NSObject {
    NSMutableArray *_fileArray;
}

@property (nonatomic, retain) NSMutableArray *fileArray;

-(void)startDownloads:(NSArray *)filesToDownload;
-(void)downloadDocument:(NSMutableArray *)documentsArray;

@end
