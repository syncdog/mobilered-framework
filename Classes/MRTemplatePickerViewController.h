//
//  MRTemplatePickerViewController.h
//  MobileRed
//
//  Created by Lion User on 26/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MRMREDMessage;

@interface MRTemplatePickerViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate> {
    MRMREDMessage *_message;
    UILabel *_templateLabel, *_linksLabel, *_urlLabel, *_phoneLabel;
    UIPickerView *_picker;
    UISwitch *_urlSwitch, *_phoneSwitch;
    NSMutableArray *_pickerElements;
}

@property (nonatomic, retain) IBOutlet UILabel *templateLabel, *linksLabel, *urlLable, *phoneLable;
@property (nonatomic, retain) IBOutlet UIPickerView *picker;
@property (nonatomic, retain) IBOutlet UISwitch *urlSwitch, *phoneSwitch;
@property (nonatomic, retain) MRMREDMessage *message;
@property (nonatomic, retain) NSMutableArray *pickerElements;

-(id)initWithMessage:(MRMREDMessage *)theMessage;
-(void)nextStep;

@end
