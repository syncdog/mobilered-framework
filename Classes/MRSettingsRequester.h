//
//  MRSettingsRequester.h
//  MobileRed
//
//  Created by Lion User on 02/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequester.h"

@interface MRSettingsRequester : MRHttpRequester 

-(void) requestSettings;

@end
