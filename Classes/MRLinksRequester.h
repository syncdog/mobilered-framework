//
//  MRLinksRequester.h
//  MobileRed
//
//  Created by Lion User on 05/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequester.h"



@interface MRLinksRequester : MRHttpRequester 

-(void)requestLinks;

@end
