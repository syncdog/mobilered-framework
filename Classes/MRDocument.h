//
//  MRDocument.h
//  MobileRed
//
//  Created by Lion User on 09/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kIdKey @"ID KEY"
#define kDownloadUrlKey @"DOWNLOAD URL"
#define kSizeKey @"SIZE"
#define kOtherIdKey @"OTHER ID"
#define kFolderKey @"FOLDER"
#define kFileNameKey @"FILENAME"

@interface MRDocument : NSObject <NSCoding> {
    NSString *_idDoc;
    NSString *_downloadUrl;
    NSString *_size;
    NSString *_otherId;
    NSString *_folder;
    NSString *_fileName;
}

@property (nonatomic, retain) NSString *idDoc;
@property (nonatomic, retain) NSString *downloadUrl;
@property (nonatomic, retain) NSString *size;
@property (nonatomic, retain) NSString *otherId;
@property (nonatomic, retain) NSString *folder;
@property (nonatomic, retain) NSString *fileName;

-(id)initWithDocString:(NSString *)documentString;
-(NSString *)createFileNameFromDownloadUrl:(NSString *)downloadUrl;

@end
