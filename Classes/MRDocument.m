//
//  MRDocument.m
//  MobileRed
//
//  Created by Lion User on 09/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRDocument.h"

@implementation MRDocument

@synthesize idDoc = _idDoc;
@synthesize downloadUrl = _downloadUrl;
@synthesize size = _size;
@synthesize otherId = _otherId;
@synthesize folder = _folder;
@synthesize fileName = _fileName;

-(id)initWithDocString:(NSString *)documentString {
    self = [super init];
    
    if (self) {
        if(! [documentString hasPrefix:@"D"]) {
            return nil;
        }
        
        NSArray *split = [documentString componentsSeparatedByString:@"|"];
        
        self.idDoc = [split objectAtIndex:1];
        self.downloadUrl = [split objectAtIndex:2];
        self.size = [split objectAtIndex:3];
        self.otherId = [split objectAtIndex:4];
        
        if ([split count] > 5) {
            self.folder = [split objectAtIndex:5];
        }
        
        self.fileName = [self createFileNameFromDownloadUrl:_downloadUrl];
    }
    
    return self;
}

-(NSString *)createFileNameFromDownloadUrl:(NSString *)downloadUrl {
    NSString *lastSlash = [downloadUrl substringFromIndex:[downloadUrl rangeOfString:@"/" options:NSBackwardsSearch].location + 1];
    return [lastSlash stringByReplacingOccurrencesOfString:@"%20" withString:@""];
}

-(void)dealloc {
    [_idDoc release];
    [_downloadUrl release];
    [_size release];
    [_otherId release];
    [_folder release];
    [_fileName release];
    
    [super dealloc];
}

#pragma NSCoding methods

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if (self) {
        self.idDoc = [aDecoder decodeObjectForKey:kIdKey];
        self.downloadUrl = [aDecoder decodeObjectForKey:kDownloadUrlKey];
        self.size = [aDecoder decodeObjectForKey:kSizeKey];
        self.otherId = [aDecoder decodeObjectForKey:kOtherIdKey];
        self.folder = [aDecoder decodeObjectForKey:kFolderKey];
        self.fileName = [aDecoder decodeObjectForKey:kFileNameKey];
    }   
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_idDoc forKey:kIdKey];
    [aCoder encodeObject:_downloadUrl forKey:kDownloadUrlKey];
    [aCoder encodeObject:_size forKey:kSizeKey];
    [aCoder encodeObject:_otherId forKey:kOtherIdKey];
    [aCoder encodeObject:_folder forKey:kFolderKey];
    [aCoder encodeObject:_fileName forKey:kFileNameKey];
}

@end
