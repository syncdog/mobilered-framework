//
//  MRMessageTypeViewController.m
//  MobileRed
//
//  Created by Lion User on 26/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRMessageTypeViewController.h"
#import "MRMREDMessage.h"
#import "MRMessageLevelViewController.h"

@implementation MRMessageTypeViewController

@synthesize pushButton = _pushButton;
@synthesize smsButton = _smsButton;
@synthesize anyButton = _anyButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
   
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc {
    [_pushButton release];
    [_smsButton release];
    [_anyButton release];
    
    [super dealloc];
}

-(void)buttonPressed:(id)sender {
    MRMREDMessage *message = [[MRMREDMessage alloc] init];
    UIButton *button = (UIButton *) sender;
    
    switch (button.tag) {
        case kPushButtonTag:
            message.type = typePushMessage;
            break;
        case kSmsButtonTag:
            message.type = typeSMS;
            break;
        case kAnyButtonTag:
            message.type = typeAnyAvailable;
            break;
        default:
            break;
    }
    
    MRMessageLevelViewController *controller = [[[MRMessageLevelViewController alloc] initWithMessageType:message] autorelease];
    
    [message release];
    
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Message Type", nil);

    _pushButton.titleLabel.text = NSLocalizedString(@"Direct Push", nil);
    _smsButton.titleLabel.text = NSLocalizedString(@"SMS", nil);
    _anyButton.titleLabel.text = NSLocalizedString(@"Any available", nil);
}

- (void)viewDidUnload
{
    self.pushButton = nil;
    self.smsButton = nil;
    self.anyButton = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
