//
//  MRFormsXMLRequester.m
//  MobileRed
//
//  Created by Lion User on 10/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRFormsXMLRequester.h"

@implementation MRFormsXMLRequester

-(void)getFormXML:(NSString *)formId {
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    self.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [_request setPostValue:kActionGetForm forKey:kParameterAction];
    [_request setPostValue:formId forKey:kParameterFormId];
    
    self.request.delegate = self;
    [_request startAsynchronous];
}

-(void)processResponse:(NSData *)data {
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *str;
    if([response hasSuffix:@"\r\n"]) {
        str = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else {
        str = response;
    }
    
    NSMutableDictionary *formsDict = [MRRuntimeStoreAccessor getFromRuntimeStore:kForms];
    
    bool wasNil = false;
    if(formsDict == nil) {
        wasNil = true;
        formsDict = [[NSMutableDictionary alloc] init];
    }
    
    NSArray *split = [str componentsSeparatedByString:@"|"];
    
    [formsDict setValue:[split objectAtIndex:2] forKey:[split objectAtIndex:1]];
    
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:formsDict forKey:kForms];
    
    if (wasNil) {
        [formsDict release];
    }
    
    [response release];
}

@end
