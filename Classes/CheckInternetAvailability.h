//
//  CheckInternetAvailability.h
//  MobileRed
//
//  Created by Lion User on 20/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>

@interface CheckInternetAvailability : NSObject

+(BOOL)hasConnectivity;

@end
