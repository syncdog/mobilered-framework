//
//  MRRuntimeStoreAccessor.h
//  MobileRed
//
//  Created by Lion User on 03/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GMAppDelegate;

@interface MRRuntimeStoreAccessor : NSObject

+(id)getFromRuntimeStore:(NSString *)key;
+(void)setInRuntimeStoreObject:(id)object forKey:(NSString *) key;

@end
