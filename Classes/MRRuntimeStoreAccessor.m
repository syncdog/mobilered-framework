//
//  MRRuntimeStoreAccessor.m
//  MobileRed
//
//  Created by Lion User on 03/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRRuntimeStoreAccessor.h"
#import "MobileRED.h"

@implementation MRRuntimeStoreAccessor 

+(id)getFromRuntimeStore:(NSString *)key {
    return [[MobileRED sharedManager].runtimeStore objectForKey:key];
}

+(void)setInRuntimeStoreObject:(id)object forKey:(NSString *)key {
    NSMutableDictionary *dict = [MobileRED sharedManager].runtimeStore;
    
    [dict setObject:object forKey:key];
}

@end
