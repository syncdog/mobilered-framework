//
//  MRDialogDelegate.h
//  MobileRed
//
//  Created by Lion User on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MRDialogDelegate <NSObject>

@required
-(void) dialogFinishedTasks;

@end
