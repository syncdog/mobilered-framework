//
//  RuntimePersistenceHelper.h
//  MobileRed
//
//  Created by Lion User on 19/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kRuntimeFile @"runtimeFile.plist"

@interface RuntimePersistenceHelper : NSObject {
    NSMutableDictionary *_dictionary;
}

@property (nonatomic, retain) NSMutableDictionary *dictionary;

-(void) writeToFile:(NSDictionary *) contents;
-(NSString *) filePath;
-(void) clearFile;

+(id)sharedInstance;

@end
