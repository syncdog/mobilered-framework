//
//  MRURLPickerViewController.h
//  MobileRed
//
//  Created by Lion User on 27/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MRMREDMessage;

@interface MRURLPickerViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate > {
    MRMREDMessage *_message;
    UILabel *_label, *_orLabel;
    UITextField *_manualLink;
    UIPickerView *_linkPicker;
    NSMutableArray *_linkArray;
    BOOL _nextPressent;
}

@property (nonatomic, retain) MRMREDMessage *message;
@property (nonatomic, retain) IBOutlet UILabel *label, *orLabel;
@property (nonatomic, retain) IBOutlet UITextField *manualLink;
@property (nonatomic, retain) IBOutlet UIPickerView *linkPicker;
@property (nonatomic, retain) NSMutableArray *linkArray;
@property (nonatomic) BOOL nextPressent;

-(id)initWithMessage:(MRMREDMessage *)theMessage andNextButton:(BOOL)hasNext;
-(void)nextStep;

@end
