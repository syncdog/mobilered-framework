//
//  MRTemplateRequester.m
//  MobileRed
//
//  Created by Lion User on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRTemplateRequester.h"
#import "MRTemplate.h"

@implementation MRTemplateRequester

-(void)requestTemplates {
    _requestIdentifier = kTemplateRequest;
    
    NSURL *url = [NSURL URLWithString:[MRRuntimeStoreAccessor getFromRuntimeStore:kURL]];
    
    self.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [_request setPostValue:kActionTemplate forKey:kParameterAction];
    
    self.request.delegate = self;
    [_request startAsynchronous];
}

-(void)processResponse:(NSData *)data {
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *str;
    if([response hasSuffix:@"\r\n"]) {
        str = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else {
        str = response;
    }
    
    NSMutableArray *templateArray = [[NSMutableArray alloc] init];
    
    NSArray *newLineSplit = [str componentsSeparatedByString:@"\r\n"];
    
    for (NSString *item in newLineSplit) {
        MRTemplate *templeate = [[MRTemplate alloc] initWithString:item];
        
        [templateArray addObject:templeate];
        
        [templeate release];
    }
    
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:templateArray forKey:kTemplates];
    
    [templateArray release];
    [response release];
}

@end
