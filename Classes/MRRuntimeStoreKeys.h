//
//  MRRuntimeStoreKeys.h
//  MobileRed
//
//  Created by Lion User on 02/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef MobileRed_MRRuntimeStoreKeys_h
#define MobileRed_MRRuntimeStoreKeys_h

#pragma register keys 

#define kRegisteredKey @"REGISTERED"
#define kOfflineModeKey @"OFFLINE MODE"
#define kRegisterEmail @"EMAIL"
#define kRegisterPhone @"PHONE"

#pragma settings keys

#define kURL @"URL"
#define kRefreshTime @"REFRESH TIME"
#define kSettingsPassword @"SETTINGS PASSWORD"
#define kMessagesLevels @"LEVELS"

#pragma Runtime Store elements keys

#define kAddressBook @"ADDRESS BOOK"
#define kLinks @"LINKS"
#define kTemplates @"TEMPLATES"
#define kConferenceCalls @"CC"
#define kDocuments @"DOCUMENTS"
#define kForms @"FORMS"

#endif
