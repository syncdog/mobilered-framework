//
//  MRTemplateRequester.h
//  MobileRed
//
//  Created by Lion User on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequester.h"

@interface MRTemplateRequester : MRHttpRequester 

-(void)requestTemplates;

@end
