//
//  GMMainMenuViewController.h
//  MobileRed
//
//  Created by Lion User on 19/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MRDialogDelegate.h"
#import "MRHttpRequesterDelegate.h"

#define kOptionsButtonTag 0
#define kContactsButtonTag 1
#define kCreateMessageButtonTag 2

@class MRRegisterDialog;
@class MRSettingsRequester;

@interface GMMainMenuViewController : UIViewController <HttpRequesterDelegate, MRDialogDelegate, UIAlertViewDelegate> 
{
    UILabel *_createMessages, *_receivedMessages, *_constacts, *_sentMessages, *_forms, *_options;
    MRSettingsRequester *_requester;
    MRRegisterDialog *_registerDialog;
    UIAlertView *_progress;
}

@property (nonatomic, retain) IBOutlet UILabel *createMessages, *receivedMessages, *constacts, *sentMessages, *forms, *options;
@property (nonatomic, retain) MRSettingsRequester *requester;
@property (nonatomic, retain) MRRegisterDialog *registerDialog;
@property (nonatomic, retain) UIAlertView *progress;

- (id)initMainViewController;
-(void) getSettings;
-(void) showRegisterDialog;

-(IBAction)buttonAction:(id)sender;

#pragma ProgressAlertView methods

-(void)showProgressAlertWithMessage:(NSString *)message;
-(void)closeProgress;

@end
