//
//  MRGroupContact.h
//  MobileRed
//
//  Created by Lion User on 04/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRContact.h"

#define kGroupIds @"GROUP IDS"
#define kApproveId @"APPROVE ID"

@interface MRGroupContact : MRContact <NSCoding> {
    NSString *_approveId;
    NSArray *_groupIds;
}

@property (nonatomic, retain) NSArray *groupIds;
@property (nonatomic, retain) NSString *approveId;

-(void)initialize;
-(void)setGroupAttributes:(NSString *)groupString;

@end
