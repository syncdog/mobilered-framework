//
//  MRTemplate.h
//  MobileRed
//
//  Created by Lion User on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kIdKey @"ID"
#define kNameKey @"NAME"
#define kTitleKey @"TITLE"
#define kMessageKey @"MESSAGE"

@interface MRTemplate : NSObject <NSCoding> {
    NSString *_idTemplate;
    NSString *_name;
    NSString *_title;
    NSString *_message;
}

@property (nonatomic, retain) NSString *idTemplate;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *message;

-(id)initWithString:(NSString *)templateString;

@end
