//
//  ViewController.h
//  MobileREDContainer
//
//  Created by Dima Yarmoshuk on 9/20/15.
//  Copyright (c) 2015 SyncDog. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MRWebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, retain) IBOutlet UIWebView *webView;

@end

