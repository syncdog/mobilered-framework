//
//  MRContactDetailsViewController.h
//  MobileRed
//
//  Created by Lion User on 24/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MRContact;

@interface MRContactDetailsViewController : UIViewController {
    UILabel *_nameLabel, *_nameValue, *_emailLabel, *_emailValue, *_phoneLabel, *_phoneValue;
    UIButton *_callButton, *_mredButton,*_sendEmail, *_sendSMSButton;
    MRContact *_contact;
}

@property (nonatomic, retain) IBOutlet UILabel *nameLabel, *nameValue, *emailLabel, *emailValue, *phoneLabel, *phoneValue; 
@property (nonatomic, retain) IBOutlet UIButton *callButton, *mredButton, *sendEmail, *sendSMSButton;
@property (nonatomic, retain) MRContact *contact;

-(id)initWithContact:(MRContact *)theContact;

@end
