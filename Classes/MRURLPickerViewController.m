//
//  MRURLPickerViewController.m
//  MobileRed
//
//  Created by Lion User on 27/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRURLPickerViewController.h"
#import "MRMREDMessage.h"
#import "MRLink.h"
#import "MRPhonePicvkerViewController.h"
#import "MRMessageReviewViewController.h"
#import "MRMessageTypeViewController.h"

@implementation MRURLPickerViewController

@synthesize message = _message;
@synthesize label = _label;
@synthesize linkPicker = _linkPicker;
@synthesize linkArray = _linkArray;
@synthesize nextPressent = _nextPressent;
@synthesize orLabel = _orLabel;
@synthesize manualLink = _manualLink;

-(id)initWithMessage:(MRMREDMessage *)theMessage andNextButton:(BOOL)hasNext {
    self = [super initWithNibName:@"MRURLPickerViewController" bundle:[NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]]];
    
    if (self) {
        self.message = theMessage;
        self.nextPressent = hasNext;
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

-(void)nextStep {
    [_manualLink resignFirstResponder];

    if ([_linkPicker selectedRowInComponent:0] != 0) {
        _message.link = [_linkArray objectAtIndex:[_linkPicker selectedRowInComponent:0] - 1];
    } else if (! [_manualLink.text isEqualToString:@""]) {
        _message.manualLink = _manualLink.text;
    }
    
    UIViewController *controller = nil;
    
    if ([_message hasPhone]) {
        controller = [[[MRPhonePicvkerViewController alloc] initWithMessage:_message andHasNext:YES] autorelease];
    } else {
        controller = [[[MRMessageReviewViewController alloc] initWithMessage:_message] autorelease];
    }
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)backStep {
    [_manualLink resignFirstResponder];
    if ([_linkPicker selectedRowInComponent:0] != 0) {
        _message.link = [_linkArray objectAtIndex:[_linkPicker selectedRowInComponent:0] - 1];
    } else if (! [_manualLink.text isEqualToString:@""]) {
        _message.manualLink = _manualLink.text;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc {
    [_message release];
    [_label release];
    [_linkPicker release];
    [_linkArray release];
    [_orLabel release];
    [_manualLink release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"URL", nil);
    // Do any additional setup after loading the view from its nib.
    
    _label.text = NSLocalizedString(@"Select Link", nil);
    _orLabel.text = NSLocalizedString(@"or Enter URL manually", nil);
    _manualLink.placeholder = NSLocalizedString(@"<URL>", nil);
    
    self.linkArray = [MRRuntimeStoreAccessor getFromRuntimeStore:kLinks];
    
    if (_message.link != nil) {
        NSUInteger location = [_linkArray indexOfObject:_message.link];
        
        [_linkPicker selectRow:location + 1 inComponent:0 animated:NO];
    } else if (_message.manualLink != nil) {
        _manualLink.text = _message.manualLink;
    }
    
    if (_nextPressent) {
        UIBarButtonItem *item =[[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Next", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(nextStep)] autorelease];
        
        [self.navigationItem setRightBarButtonItem:item animated:YES];
    } else {
        UIButton *backButton = [UIButton buttonWithType:101];
        [backButton addTarget:self action:@selector(backStep) forControlEvents:UIControlEventTouchUpInside];
        [backButton setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateNormal];
        
        UIBarButtonItem *item1 = [[[UIBarButtonItem alloc] initWithCustomView:backButton] autorelease];
        
        [self.navigationItem setLeftBarButtonItem:item1 animated:YES];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

    self.label = nil;
    self.linkPicker = nil;
    self.linkArray = nil;
    self.orLabel = nil;
    self.manualLink = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - PickerDelegate methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (row == 0) {
        return NSLocalizedString(@"None", nil);
    } else {
        MRLink *link = [_linkArray objectAtIndex:row - 1];
        return link.name;
    }
}

#pragma mark - PickerDataSource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_linkArray count] + 1;
}

#pragma mark -  Keyboard appearence notification && TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField; {
    return [textField resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - 150, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 150, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

@end
