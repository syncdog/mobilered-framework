//
//  MRContact.h
//  MobileRed
//
//  Created by Lion User on 04/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma NSCoding keys

#define kID @"id"
#define kName @"name"
    
#pragma Access Levels

typedef enum {
    noAccess = -1,
    accessReader,
    accessCreator,
    accessApprover
} AcessLevels;

@interface MRContact : NSObject <NSCoding> {
    NSString *_idContact;
    NSString *_name;
    BOOL _cecked;
}

@property (nonatomic, retain) NSString *idContact;
@property (nonatomic, retain) NSString *name;
@property (nonatomic) BOOL cecked;

@end
