//
//  MRConferenceCallRequester.m
//  MobileRed
//
//  Created by Lion User on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRConferenceCallRequester.h"
#import "MRCCs.h"

@implementation MRConferenceCallRequester

-(void)requestCCs {
    _requestIdentifier = kCCRequest;
    
    NSURL *url = [NSURL URLWithString:[MRRuntimeStoreAccessor getFromRuntimeStore:kURL]];
    
    self.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [_request setPostValue:kActionCC forKey:kParameterAction];
    
    self.request.delegate = self;
    [_request startAsynchronous];
}

-(void)processResponse:(NSData *)data {
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *str;
    if([response hasSuffix:@"\r\n"]) {
        str = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else {
        str = response;
    }
    
    NSMutableArray *ccArray = [[NSMutableArray alloc] init];
    
    NSArray *newLineSplit = [str componentsSeparatedByString:@"\r\n"];
    
    for (NSString *item in newLineSplit) {
        MRCCs *cc = [[MRCCs alloc] initWithString:item];
        
        [ccArray addObject:cc];
        
        [cc release];
    }
    
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:ccArray forKey:kConferenceCalls];
    
    [ccArray release];
    [response release];
}

@end
