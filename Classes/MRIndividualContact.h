//
//  MRIndividualContact.h
//  MobileRed
//
//  Created by Lion User on 04/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRContact.h"

#define kAccessLevel @"ACCESS LEVEL"
#define kEmail @"EMAIL"
#define kPin @"PIN"
#define kPhoneNumber @"PHONE NUMBER"
#define kApproveId @"APPROVE ID"
#define kSmsEnabled @"SMS ENABLED"

@interface MRIndividualContact : MRContact <NSCoding> {
    NSString *_accessLevel;
    NSString *_email;
    NSString *_pin;
    NSString *_phoneNumber;
    NSString *_approveId;
    NSString *_smsEnabled;
}

-(void)setContactAttributes:(NSString *)contactString;

@property (nonatomic, retain) NSString *accessLevel;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *pin;
@property (nonatomic, retain) NSString *phoneNumber;
@property (nonatomic, retain) NSString *approveId;
@property (nonatomic, retain) NSString *smsEnabled;

@end
