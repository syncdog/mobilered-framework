//
//  MRPhonePicvkerViewController.h
//  MobileRed
//
//  Created by Marius Gherman on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MRMREDMessage;

@interface MRPhonePicvkerViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate> {
    MRMREDMessage *_message;
    UILabel *_label, *_orLabel;
    UITextField *_dialInNumber, *_accessCode;
    UIPickerView *_phonePicker;
    NSMutableArray *_phoneArray;
    BOOL _nextPressent;
    UITextField *_activeTF;
}

@property (nonatomic, retain) MRMREDMessage *message;
@property (nonatomic, retain) IBOutlet UILabel *label, *orLabel;
@property (nonatomic, retain) IBOutlet UIPickerView *phonePicker;
@property (nonatomic, retain) NSMutableArray *phoneArray;
@property (nonatomic) BOOL nextPressent;
@property (nonatomic, retain) IBOutlet UITextField *dialInNumber, *accessCode;
-(id)initWithMessage:(MRMREDMessage *)theMessage andHasNext:(BOOL)hasNext;

@end
