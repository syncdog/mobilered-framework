//
//  RuntimePersistenceHelper.m
//  MobileRed
//
//  Created by Lion User on 19/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RuntimePersistenceHelper.h"

static RuntimePersistenceHelper *sharedInstance = nil;

@implementation RuntimePersistenceHelper

@synthesize dictionary = _dictionary;

#pragma class methods

- (id)init {
    if (self = [super init]) {
        NSString *fileContents = [self filePath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:fileContents]) {
            self.dictionary = [[[NSMutableDictionary alloc] initWithContentsOfFile:fileContents] autorelease];
        } else {
            NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
            self.dictionary = mutDict;
            
            [mutDict release];
        }
    }
    
    return self;
}

-(NSString *)filePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [paths objectAtIndex:0];
    return [docDirectory stringByAppendingPathComponent:kRuntimeFile];
}

-(void)writeToFile:(NSDictionary *)contents {
    [contents writeToFile:[self filePath] atomically:YES];
    
    [_dictionary setDictionary:contents];
}

-(void)clearFile {
    [_dictionary removeAllObjects];
    [self writeToFile:_dictionary];
}

#pragma Singleton methods
+(id)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+(id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

-(id)retain {
    return self;
}

-(NSUInteger)retainCount {
    return NSUIntegerMax;
}

-(id)autorelease {
    return self;
}

@end
