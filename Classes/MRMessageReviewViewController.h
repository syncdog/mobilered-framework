//
//  MRMessageReviewViewController.h
//  MobileRed
//
//  Created by Marius Gherman on 5/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kLevelSliderTag 0
#define kDeliverySliderTag 1

#define kLinkButtonTag 0
#define kCCButtonTag 1
#define kSelectRecepientsButtonTag 2

@class MRMREDMessage;

@interface MRMessageReviewViewController : UIViewController {
    MRMREDMessage *_message;
    UILabel *_toLabel, *_recepientsLabel, *_sendCCLabel, *_deliverVoiceLabel, *_messageLevelLabel, *_messageLevelValueLabel, *_deliveryLabel, *_deliveryValueLabel, *_attachmentsLabel;
    UIButton *_recepientsButton, *_linkButton, *_ccButton;
    UISwitch *_ccSwitch, *_deliverVoiceSwitch;
    UISlider *_levelSlider, *_deliverySlider;
}

@property (nonatomic, retain) MRMREDMessage *message;
@property (nonatomic, retain) IBOutlet UILabel *toLabel, *recepientsLabel, *sendCCLabel, *deliverVoiceLabel, *messageLevelLabel, *messageLevelValueLabel, *deliveryLabel, *deliveryValueLabel, *attachmentsLabel;
@property (nonatomic, retain) IBOutlet UIButton *recepientsButton, *linkButton, *ccButton;
@property (nonatomic, retain) IBOutlet UISwitch *ccSwitch, *deliverVoiceSwitch;
@property (nonatomic, retain) IBOutlet UISlider *levelSlider, *deliverySlider;

-(id)initWithMessage:(MRMREDMessage *)theMessage;
-(void)changeUiToMatchMessageSettings;
-(IBAction)sliderChanged:(id)sender;
-(IBAction)buttonPressed:(id)sender;

-(void)changeLevel:(NSInteger)level changeSlider:(BOOL)change;
-(void)changeDelivery:(NSInteger)level changeSlider:(BOOL)change;

@end
