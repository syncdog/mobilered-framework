//
//  MRDocumentListRequester.m
//  MobileRed
//
//  Created by Lion User on 09/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRDocumentListRequester.h"
#import "MRDocument.h"
#import "MRFileDownloader.h"

@implementation MRDocumentListRequester

-(void)requestDocuments {
    _requestIdentifier = kDocumentsRequest;
    
    NSURL *url = [NSURL URLWithString:[MRRuntimeStoreAccessor getFromRuntimeStore:kURL]];
    
    self.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [_request setPostValue:kActionDocuments forKey:kParameterAction];
    
    self.request.delegate = self;
    [_request startAsynchronous];
}

-(void)processResponse:(NSData *)data {
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *str;
    if([response hasSuffix:@"\r\n"]) {
        str = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else {
        str = response;
    }
    
    NSMutableArray *documentsArray = [[NSMutableArray alloc] init];
    
    NSArray *newLineSplit = [str componentsSeparatedByString:@"\r\n"];
    
    for (NSString *item in newLineSplit) {
        MRDocument *doc = [[MRDocument alloc] initWithDocString:item];
        
        if(doc != nil) {
            [documentsArray addObject:doc];
        }
        
        [doc release];
    }
    
    NSArray *downloadArray = [self compareDocuments:documentsArray];
    
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:documentsArray forKey:kDocuments];
    
    MRFileDownloader *down = [[MRFileDownloader alloc] init];
     
    [down startDownloads:downloadArray];
    
    [down release];
    [documentsArray release];
    [response release];
}

-(NSArray *)compareDocuments:(NSMutableArray *)array {
    NSArray *receivedDocs = array;
    
    NSArray *inStore = [MRRuntimeStoreAccessor getFromRuntimeStore:kDocuments];
    
    NSArray *toReturn = nil;
    
    if(inStore != nil && [inStore count] > 0) {
        NSMutableDictionary *toDownload = [[NSMutableDictionary alloc] init];
        
        for (int i = 0 ; i < [receivedDocs count] ; i++) {
            MRDocument *docReceived = [receivedDocs objectAtIndex:i];
            
            for (MRDocument *doc in inStore) {
                if([doc.idDoc isEqualToString:docReceived.idDoc]) {
                    if(! [doc.size isEqualToString:docReceived.size]) {
                        [toDownload setObject:docReceived forKey:docReceived.idDoc];
                        break;
                    } else {
                        [toDownload setObject:[NSNumber numberWithInt:0] forKey:docReceived.idDoc];
                        break;
                    }
                } 
            }
        }
        
        NSArray *keys = [toDownload allKeys];
        for (MRDocument *doc in receivedDocs) {
            if(![keys containsObject:doc.idDoc]) {
                [toDownload setObject:doc forKey:doc.idDoc];
            }
        }
        
        keys = [toDownload allKeys];
        for (NSString *key in keys) {
            if ([[toDownload objectForKey:key] isEqual:0]) {
                [toDownload removeObjectForKey:key];
            }
        }
        
        toReturn = [toDownload allValues];
        
        [toDownload release];
    } else {
        toReturn = receivedDocs;
    }
    
    return toReturn ;
}

@end
