//
//  PHttpRequester.h
//  MobileRed
//
//  Created by Lion User on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HttpRequesterDelegate <NSObject> 

@required 
    -(void) wsCallFinished:(BOOL)wasOk withResponse:(NSNumber *)responseCode;

@optional
    -(void) wsCallFinished:(BOOL)wasOk withResponse:(NSNumber *)responseCode forRequest:(NSInteger)request; 

@end
