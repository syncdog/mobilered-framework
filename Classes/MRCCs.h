//
//  MRCCs.h
//  MobileRed
//
//  Created by Lion User on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kIdCCKey @"ID"
#define kNameKey @"NAME"
#define kNumberKey @"NUMBER"
#define kOtherNumberKey @"OTHER NUMBER"

@interface MRCCs : NSObject <NSCoding> {
    NSString *_idCC;
    NSString *_name;
    NSString *_number;
    NSString *_otherNumber;
}

@property (nonatomic, retain) NSString *idCC;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *number;
@property (nonatomic, retain) NSString *otherNumber;

-(id)initWithString:(NSString *)ccString;

@end
