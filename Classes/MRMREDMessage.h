//
//  MRMREDMessage.h
//  MobileRed
//
//  Created by Lion User on 26/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRLink.h"
#import "MRCCs.h"

typedef enum  {
    typePushMessage = 0,
    typeSMS,
    typeAnyAvailable
} MessageType;

typedef enum {
    levelEmergency = 1,
    levelWarning,
    levelInformation
} MessageLevel;

@class MRTemplate;

@interface MRMREDMessage : NSObject {
    MessageType _type;
    MessageLevel _level;
    MRTemplate *_templateT;
    BOOL _hasLink;
    MRLink *_link;
    BOOL _hasPhone;
    MRCCs *_cc;
    NSString *_subject;
    NSString *_message;
    
    NSString *_manualLink;
    NSString *_dialInNumber;
    NSString *_accessCode;
    
    BOOL _includeVoice;
    BOOL _sendCC;
}

@property (nonatomic) MessageType type; 
@property (nonatomic) MessageLevel level;

@property (nonatomic) BOOL hasLink;
@property (nonatomic) BOOL hasPhone;
@property (nonatomic) BOOL includeVoice;
@property (nonatomic) BOOL sendCC;

@property (nonatomic, retain) MRTemplate *templateT;
@property (nonatomic, retain) MRLink *link;
@property (nonatomic, retain) MRCCs *cc;
@property (nonatomic, retain) NSString *subject;
@property (nonatomic, retain) NSString *message;

@property (nonatomic, retain) NSString *manualLink;
@property (nonatomic, retain) NSString *dialInNumber;
@property (nonatomic, retain) NSString *accessCode;

-(NSString *)messageLink;
-(NSString *)messagePhone;
-(int)generateMessageId;

@end
