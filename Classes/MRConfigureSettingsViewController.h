//
//  MRConfigureSettingsViewController.h
//  MobileRed
//
//  Created by Lion User on 10/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRConfigureSettingsViewController : UIViewController {
    UILabel *_url, *_sleepTime, *_sleepTimeValue, *_imei, *_emailLabel, *_emailValue, *_accessLevelName, *_accessLevelValue;
}

@property (nonatomic, retain) IBOutlet UILabel *url, *sleepTime, *sleepTimeValue, *imei, *emailLabel, *emailValue, *accessLevelName, *accessLevelValue;

-(IBAction)changeUrl:(id)sender;
-(IBAction)changeSleepTime:(id)sender;

@end
