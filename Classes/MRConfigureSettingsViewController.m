//
//  MRConfigureSettingsViewController.m
//  MobileRed
//
//  Created by Lion User on 10/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRConfigureSettingsViewController.h"

@implementation MRConfigureSettingsViewController

@synthesize url = _url, sleepTime = _sleepTime, sleepTimeValue = _sleepTimeValue, imei = _imei, emailLabel = _emailLabel, emailValue = _emailValue, accessLevelName = _accessLevelName, accessLevelValue = _accessLevelValue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc {
    [_url release];
    [_sleepTime release];
    [_sleepTimeValue release];
    [_imei release];
    [_emailValue release];
    [_emailLabel release];
    [_accessLevelName release];
    [_accessLevelValue release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _url.text = [MRRuntimeStoreAccessor getFromRuntimeStore:kURL];
    _sleepTime.text = NSLocalizedString(@"Sleep time (minutes)", nil);
    _sleepTimeValue.text = [MRRuntimeStoreAccessor getFromRuntimeStore:kRefreshTime];
    _imei.text = [UIDevice currentDevice].identifierForVendor.UUIDString;
    _emailLabel.text = NSLocalizedString(@"Email", nil);
    _emailValue.text = [MRRuntimeStoreAccessor getFromRuntimeStore:kRegisterEmail];
    _accessLevelName.text = NSLocalizedString(@"Access level", nil);
    _accessLevelValue.text = [MRRuntimeStoreAccessor getFromRuntimeStore:kMessagesLevels];
}

- (void)viewDidUnload
{
    _url = nil;
    _sleepTime = nil;
    _sleepTimeValue = nil;
    _imei = nil;
    _emailLabel = nil;
    _emailValue = nil;
    _accessLevelName = nil;
    _accessLevelValue = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
