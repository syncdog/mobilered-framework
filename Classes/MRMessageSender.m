//
//  MRMessageSender.m
//  MobileRed
//
//  Created by Marius Gherman on 5/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRMessageSender.h"
#import "MRIndividualContact.h"
#import "ASIFormDataRequest.h"
#import "MRMREDMessage.h"
#import "MRAddressBook.h"

@implementation MRMessageSender

@synthesize message = _message;

-(id)initWithMessage:(MRMREDMessage *)theMessage {
    self = [super init];
    
    if (self) {
        self.message = theMessage;
    }
    
    return self;
}

-(void)sendMessage {
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:kBaseUrl]];
    
    MRIndividualContact *me = [self findMe];
    if (me != nil) {
        if ([me.accessLevel intValue] == accessCreator) {
            [request setPostValue:kActionPushMessage forKey:kParameterAction];
        } else {
            [request setPostValue:kActionApproveMessage forKey:kParameterAction];
        }
        
        [request setPostValue:me.email forKey:kMessagePushFrom];
        [request setPostValue:me.name forKey:kMessagePushName];
        [request setPostValue:_message.subject forKey:kMessagePushSubject];
        [request setPostValue:_message.message forKey:kMessagePushMessage];
        
        NSString *str0 = [[[NSString alloc] initWithFormat:@"%i", [_message generateMessageId]] autorelease];
        [request setPostValue:str0 forKey:kMessagePushMessageId];
        
        [request setPostValue:_message.includeVoice ? @"TRUE" : @"FALSE" forKey:kMessagePushVoice];
        
        NSString *str1 = [[[NSString alloc] initWithFormat:@"%i", _message.level] autorelease];
        [request setPostValue:str1 forKey:kMessagePushLevel];
        
        if ([_message messageLink] != nil) {
            [request setPostValue:[_message messageLink] forKey:kMessagePushLink];
        }
        
        if ([_message messagePhone] != nil) {
            [request setPostValue:[_message messagePhone] forKey:kMessagePushPhone];
        }
        
        NSString *toString = [self getRecepients];
        
        [request setPostValue:toString forKey:kMessagePushTo];
        
        UIDevice *imeiHelper =[UIDevice currentDevice];
        NSString *imei = imeiHelper.identifierForVendor.UUIDString;

        [request setPostValue:imei forKey:kParameterIMEI];
        [request setPostValue:kiOSIdentifier forKey:kParameterOS];
        
        // set the app version
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        [request setPostValue:version forKey:kParameterAppVersion];
        
        [request startAsynchronous];
    }
}

-(MRIndividualContact *)findMe {
    MRAddressBook *book = [MRRuntimeStoreAccessor getFromRuntimeStore:kAddressBook];
    
    UIDevice *imeiHelper =[UIDevice currentDevice];
    NSString *imei = imeiHelper.identifierForVendor.UUIDString;
    
    if (book != nil) {
        NSMutableArray *arr = book.individualContacts;
        
        for (MRIndividualContact *ctc in arr) {
            if ([ctc.pin isEqualToString:imei]) {
                return ctc;
            }
        }
    }
    
    return nil;
}

-(NSString *)getRecepients {
#warning incomplete
    return nil;
}

@end
