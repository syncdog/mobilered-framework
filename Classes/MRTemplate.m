//
//  MRTemplate.m
//  MobileRed
//
//  Created by Lion User on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRTemplate.h"

@implementation MRTemplate 

@synthesize idTemplate = _idTemplate;
@synthesize name = _name;
@synthesize title = _title;
@synthesize message = _message;

-(id)initWithString:(NSString *)templateString {
    self = [super init];
    
    if (self) {
        NSArray *split = [templateString componentsSeparatedByString:@"|"];
        
        self.idTemplate = [split objectAtIndex:1];
        self.name = [split objectAtIndex:2];
        self.title = [split objectAtIndex:3];
        self.message = [split objectAtIndex:4];
    }
    
    return self;
}

-(void)dealloc {
    [_idTemplate release];
    [_name release];
    [_title release];
    [_message release];
    
    [super dealloc];
}

#pragma NSCoding methods

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if (self) {
        self.idTemplate = [aDecoder decodeObjectForKey:kIdKey];
        self.name = [aDecoder decodeObjectForKey:kNameKey];
        self.title = [aDecoder decodeObjectForKey:kTitleKey];
        self.message = [aDecoder decodeObjectForKey:kMessageKey];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_idTemplate forKey:kIdKey];
    [aCoder encodeObject:_name forKey:kNameKey];
    [aCoder encodeObject:_title forKey:kTitleKey];
    [aCoder encodeObject:_message forKey:kMessageKey];
}

@end
