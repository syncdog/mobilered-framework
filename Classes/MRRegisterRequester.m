//
//  MRRegisterRequester.m
//  MobileRed
//
//  Created by Lion User on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRRegisterRequester.h"

@implementation MRRegisterRequester

-(void)requestRegistrationWithEmail:(NSString *)email andPhone:(NSString *)phone {
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    super.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [super.request setPostValue:kActionRegister forKey:kParameterAction];
    
    super.request.delegate = self;
    [super.request startAsynchronous];
}

-(void)processResponse:(NSData *)data {
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:@"REGISTERED" forKey:kRegisteredKey];
    }

@end
