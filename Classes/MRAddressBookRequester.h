//
//  MRAddressBookRequester.h
//  MobileRed
//
//  Created by Lion User on 03/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequester.h"

@interface MRAddressBookRequester : MRHttpRequester

-(void)requestAddressBook;

@end
