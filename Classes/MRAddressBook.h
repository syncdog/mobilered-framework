//
//  MRAddressBook.h
//  MobileRed
//
//  Created by Lion User on 04/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kIndividiualContacts @"INDIVIDUAL CONTACTS"
#define kGroupContacts @"GROUP CONTACTS"

@interface MRAddressBook : NSObject <NSCoding> {
    NSMutableArray *_individualContacts;
    NSMutableArray *_groupContacts;
}

@property (nonatomic, retain) NSMutableArray *individualContacts;
@property (nonatomic, retain) NSMutableArray *groupContacts;

-(void)parseContacts:(NSString *)contacts;
-(NSArray *)getContacts;

@end
