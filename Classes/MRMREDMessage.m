//
//  MRMREDMessage.m
//  MobileRed
//
//  Created by Lion User on 26/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRMREDMessage.h"
#import "MRTemplate.h"
#import "MRLink.h"
#import "MRCCs.h"

@implementation MRMREDMessage

@synthesize type = _type;
@synthesize level = _level;
@synthesize templateT = _templateT;
@synthesize hasLink = _hasLink;
@synthesize hasPhone = _hasPhone;
@synthesize link = _link;
@synthesize cc = _cc;
@synthesize subject = _subject;
@synthesize message = _message;

@synthesize manualLink = _manualLink;
@synthesize dialInNumber = _dialInNumber;
@synthesize accessCode = _accessCode;

@synthesize includeVoice = _includeVoice;
@synthesize sendCC = _sendCC;

-(NSString *)messageLink {
    if (_link != nil) {
        return _link.name;
    } else if (_manualLink != nil) {
        return _manualLink;
    }
    
    return nil;
}

-(NSString *)messagePhone {
    if (_cc != nil) {
        return _cc.name;
    } else if (_dialInNumber != nil) {
        return _dialInNumber;
    }
    
    return nil;
}

-(int)generateMessageId {
    return (int)1 + arc4random() % NSIntegerMax;
}

-(void)dealloc {
    [_templateT release];
    [_link release];
    [_cc release];
    
    [_subject release];
    [_message release];
    
    [_manualLink release];
    [_dialInNumber release];
    [_accessCode release];
    
    [super dealloc];
}

@end
