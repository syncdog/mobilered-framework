//
//  MRTemplatePickerViewController.m
//  MobileRed
//
//  Created by Lion User on 26/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRTemplatePickerViewController.h"
#import "MRMREDMessage.h"
#import "MRTemplate.h"
#import "MRURLPickerViewController.h"
#import "MRPhonePicvkerViewController.h"
#import "MRMessageReviewViewController.h"
#import "MRMessageTypeViewController.h"
#import "MRRuntimeStoreAccessor.h"

@implementation MRTemplatePickerViewController

@synthesize message = _message;

@synthesize templateLabel = _templateLabel;
@synthesize linksLabel = _linksLabel;
@synthesize urlLable = _urlLabel;
@synthesize phoneLable = _phoneLabel;
@synthesize picker = _picker;
@synthesize urlSwitch = _urlSwitch;
@synthesize phoneSwitch = _phoneSwitch;

@synthesize pickerElements = _pickerElements;

-(id)initWithMessage:(MRMREDMessage *)theMessage {
    self = [super initWithNibName:@"MRTemplatePickerViewController" bundle:[NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]]];
    
    if (self) {
        self.message = theMessage;
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)nextStep {
    UIViewController *controller = nil;
    
    if ([_urlSwitch isOn]) {
        _message.hasLink = true;
    } else {
        _message.hasLink = false;
    }
    
    if ([_phoneSwitch isOn]) {
        _message.hasPhone = true;
    } else {
        _message.hasPhone = false;
    }
    
    if ([_picker selectedRowInComponent:0] != 0) {
        _message.templateT = [_pickerElements objectAtIndex:[_picker selectedRowInComponent:0]];
    } else {
        _message.templateT = nil;
    }
    
    if([_message hasLink]) {
        controller = [[[MRURLPickerViewController alloc] initWithMessage:_message andNextButton:YES] autorelease];
    } else if ([_message hasPhone]) {
        controller = [[[MRPhonePicvkerViewController alloc] initWithMessage:_message andHasNext:YES] autorelease];
    } else {
        controller = [[[MRMessageReviewViewController alloc] initWithMessage:_message] autorelease];
    }
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)dealloc {
    [_message release];
    [_pickerElements release];
    
    [_templateLabel release];
    [_linksLabel release];
    [_urlLabel release];
    [_phoneLabel release];
    [_picker release];
    [_urlSwitch release];
    [_phoneSwitch release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Template", nil);
    
    self.pickerElements = [MRRuntimeStoreAccessor getFromRuntimeStore:kTemplates];
    
    _templateLabel.text = NSLocalizedString(@"Select template", nil);
    _linksLabel.text = NSLocalizedString(@"Types of links to include", nil);
    _urlLabel.text = NSLocalizedString(@"URL", nil);
    _phoneLabel.text = NSLocalizedString(@"Phone", nil);
    
    UIBarButtonItem *item =[[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Next", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(nextStep)] autorelease];
    
    [self.navigationItem setRightBarButtonItem:item animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.pickerElements = nil;
    
    self.templateLabel = nil;
    self.linksLabel = nil;
    self.urlLable = nil;
    self.phoneLable = nil;
    self.picker = nil;
    self.urlSwitch = nil;
    self.phoneSwitch = nil;
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Picker DataSource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_pickerElements count] + 1;
}

#pragma mark - Picker Delegate methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (row == 0) {
        return NSLocalizedString(@"None", nil);
    } else {
        MRTemplate *template = [_pickerElements objectAtIndex:row - 1];
        
        return template.name;
    }
}

@end
