//
//  MRLink.m
//  MobileRed
//
//  Created by Lion User on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRLink.h"

@implementation MRLink

@synthesize idUrl = _idUrl;
@synthesize name = _name;
@synthesize url = _url;

-(id)initWithString:(NSString *)linkString {
    self = [super init];
    
    if(self) {
        NSArray *split = [linkString componentsSeparatedByString:@"|"];
        
        self.idUrl = [split objectAtIndex:1];
        self.name = [split objectAtIndex:2];
        self.url = [split objectAtIndex:3];
    }
    
    return self;
}

-(void)dealloc {
    [_idUrl release];
    [_name release];
    [_url release];
    
    [super dealloc];
}

#pragma NSCoding methods

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if(self) {
        self.idUrl = [aDecoder decodeObjectForKey:kIdUrlKey];
        self.name = [aDecoder decodeObjectForKey:kNameKey];
        self.url = [aDecoder decodeObjectForKey:kUrlKey];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_idUrl forKey:kIdUrlKey];
    [aCoder encodeObject:_name forKey:kNameKey];
    [aCoder encodeObject:_url forKey:kUrlKey];
}

@end
