//
//  MRContactsViewController.h
//  MobileRed
//
//  Created by Lion User on 23/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MRContact;

@interface MRContactsViewController : UITableViewController <UIActionSheetDelegate> {
    NSArray *_contactsArray;
    NSArray *_sectionNames;
    MRContact *_theContact;
    BOOL _isCheckable;
}

@property (nonatomic, retain) NSArray *contactsArray;
@property (nonatomic, retain) NSArray *sectionNames;
@property (nonatomic, retain) MRContact *theContact;
@property (nonatomic) BOOL isCheckable;

-(id)initWithStyle:(UITableViewStyle)style isCheckable:(BOOL)checkable;

@end
