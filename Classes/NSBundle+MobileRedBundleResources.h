//
//  NSBundle+MobileRedBundleResources.h
//  MobileRed
//
//  Created by Dima Yarmoshuk on 9/20/15.
//
//

#import <Foundation/Foundation.h>

@interface NSBundle (MobileRedBundleResources)

+ (NSBundle*)mobileRedResourcesBundle;

@end
