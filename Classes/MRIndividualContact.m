//
//  MRIndividualContact.m
//  MobileRed
//
//  Created by Lion User on 04/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRIndividualContact.h"

@implementation MRIndividualContact

@synthesize accessLevel = _accessLevel;
@synthesize email = _email;
@synthesize pin = _pin;
@synthesize phoneNumber = _phoneNumber;
@synthesize approveId = _approveId;
@synthesize smsEnabled = _smsEnabled;

-(void)setContactAttributes:(NSString *)contactString {
    NSArray *array = [contactString componentsSeparatedByString:@"|"];
    
    self.idContact = [array objectAtIndex:1];
	self.name = [array objectAtIndex:3];
    self.accessLevel = [array objectAtIndex:2];
    self.email = [array objectAtIndex:4];
    self.pin = [array objectAtIndex:5];
    self.phoneNumber = [array objectAtIndex:6];
    
    if ([array count] > 8) {
        self.approveId = [array objectAtIndex:7];
        self.smsEnabled = [array objectAtIndex:8];	
    }
}

-(void)dealloc {
    [_accessLevel release];
    [_email release];
    [_pin release];
    [_phoneNumber release];
    [_approveId release];
    [_smsEnabled release];
    
    [super dealloc];
}

#pragma NScoding methods

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.idContact = [aDecoder decodeObjectForKey:kAccessLevel];
        self.name = [aDecoder decodeObjectForKey:kEmail];
        self.accessLevel = [aDecoder decodeObjectForKey:kPin];
        self.phoneNumber = [aDecoder decodeObjectForKey:kPhoneNumber];
        self.approveId = [aDecoder decodeObjectForKey:kApproveId];
        self.smsEnabled = [aDecoder decodeObjectForKey:kSmsEnabled];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_accessLevel forKey:kAccessLevel];
    [aCoder encodeObject:_email forKey:kEmail];
    [aCoder encodeObject:_pin forKey:kPin];
    [aCoder encodeObject:_phoneNumber forKey:kPhoneNumber];
    [aCoder encodeObject:_approveId forKey:kApproveId];
    [aCoder encodeObject:_smsEnabled forKey:kSmsEnabled];
}

@end
