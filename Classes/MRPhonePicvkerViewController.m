//
//  MRPhonePicvkerViewController.m
//  MobileRed
//
//  Created by Marius Gherman on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRPhonePicvkerViewController.h"
#import "MRMREDMessage.h"
#import "MRCCs.h"
#import "MRMessageReviewViewController.h"
#import "MRMessageTypeViewController.h"

@implementation MRPhonePicvkerViewController

@synthesize message = _message;
@synthesize label = _label;
@synthesize phonePicker = _phonePicker;
@synthesize phoneArray = _phoneArray;
@synthesize nextPressent = _nextPressent;
@synthesize orLabel = _orLabel;
@synthesize dialInNumber = _dialInNumber;
@synthesize accessCode = _accessCode;

-(id)initWithMessage:(MRMREDMessage *)theMessage andHasNext:(BOOL)hasNext {
    self = [super initWithNibName:@"MRPhonePicvkerViewController" bundle:[NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]]];
    
    if (self) {
        self.message = theMessage;
        self.nextPressent = hasNext;
    }
    
    return self;
}

-(void)nextStep {
    
    [_activeTF resignFirstResponder];
    
    if ([_phonePicker selectedRowInComponent:0] != 0) {
        _message.cc = [_phoneArray objectAtIndex:[_phonePicker selectedRowInComponent:0] - 1];
    } else if (_dialInNumber.text != nil && _accessCode.text != nil) {
        _message.accessCode = _accessCode.text;
        _message.dialInNumber = _dialInNumber.text;
    }
    
    UIViewController *controller = [[[MRMessageReviewViewController alloc] initWithMessage:_message] autorelease];
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)backStep {
    
    [_activeTF resignFirstResponder];

    
    if ([_phonePicker selectedRowInComponent:0] != 0) {
        _message.cc = [_phoneArray objectAtIndex:[_phonePicker selectedRowInComponent:0] - 1];
    } else if (_dialInNumber.text != nil && _accessCode.text != nil) {
        _message.accessCode = _accessCode.text;
        _message.dialInNumber = _dialInNumber.text;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc {
    [_message release];
    [_label release];
    [_phonePicker release];
    [_phoneArray release];
    [_orLabel release];
    [_dialInNumber release];
    [_accessCode release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Phone CC", nil);
    // Do any additional setup after loading the view from its nib.
    
    _label.text = NSLocalizedString(@"Select Phone CC", nil);
    
    self.phoneArray = [MRRuntimeStoreAccessor getFromRuntimeStore:kConferenceCalls];

    if (_message.cc != nil) {
        NSUInteger location = [_phoneArray indexOfObject:_message.cc];
        
        [_phonePicker selectRow:location + 1 inComponent:0 animated:NO];
    } else if (_message.accessCode != nil && _message.dialInNumber != nil) {
        _dialInNumber.text = _message.dialInNumber;
        _accessCode.text = _message.accessCode;
    }
    
    if (_nextPressent) {
        UIBarButtonItem *item =[[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Next", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(nextStep)] autorelease];
        
        [self.navigationItem setRightBarButtonItem:item animated:YES];
    } else {
        UIButton *backButton = [UIButton buttonWithType:101];
        [backButton addTarget:self action:@selector(backStep) forControlEvents:UIControlEventTouchUpInside];
        [backButton setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateNormal];
        
        UIBarButtonItem *item1 = [[[UIBarButtonItem alloc] initWithCustomView:backButton] autorelease];
        
        [self.navigationItem setLeftBarButtonItem:item1 animated:YES];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    UITapGestureRecognizer* pickerGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [_phonePicker addGestureRecognizer:pickerGestureRecognizer];
    //[_phonePicker addGestureRecognizer:tapGestureRecognizer];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

    self.label = nil;
    self.phonePicker = nil;
    self.phoneArray = nil;
    self.orLabel = nil;
    self.dialInNumber = nil;
    self.accessCode = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - PickerDelegate methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (row == 0) {
        return NSLocalizedString(@"None", nil);
    } else {
        MRCCs *phone = [_phoneArray objectAtIndex:row -1];
        return phone.name;
    }
}

#pragma mark - PickerDataSource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_phoneArray count] + 1;
}

#pragma mark -  Keyboard appearence notification && TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField; {
    return [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _activeTF = textField;
}// became first responder


- (void)keyboardWillShow:(NSNotification*)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - 200, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 200, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

-(void)handleTap:(UITapGestureRecognizer*)tapRecognizer
{
    if(tapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        [_activeTF resignFirstResponder];
        //Figure out where the user tapped
    }
    
}


@end
