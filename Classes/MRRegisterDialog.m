//
//  RegisterDialog.m
//  MobileRed
//
//  Created by Lion User on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRRegisterDialog.h"
#import "MRRegisterRequester.h"

@implementation MRRegisterDialog

@synthesize phoneField, emailField, alert, requester;

-(id)initDialogWithDelegate:(id<MRDialogDelegate>)delegate {
    self = [super init];
    
    if(self) {
        UIAlertView *alrt = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Register", nil) message:@"\n\n\n\n" delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Register", nil), nil];
        alrt.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
        self.alert = alrt;
        [[alrt textFieldAtIndex:0] setPlaceholder:NSLocalizedString(@"Email", nil)];
        [[alrt textFieldAtIndex:1] setPlaceholder:NSLocalizedString(@"Phone No.", nil)];
        alrt.tag = kRegisterDialog;

        [alrt release];
        
        _delegate = delegate;
        
        //[self addFields];
    }
    return self;    
}

-(void)addFields {
    UITextField *tf1 = [[UITextField alloc] initWithFrame:CGRectMake(16, 50, 252 , 28)];
    
    self.emailField = tf1;
    [tf1 release];
    
    emailField.font = [UIFont systemFontOfSize:20];
    emailField.backgroundColor = [UIColor whiteColor];
    emailField.keyboardAppearance = UIKeyboardAppearanceAlert;
    emailField.keyboardType = UIKeyboardTypeEmailAddress;
    emailField.delegate = self;
    emailField.placeholder = NSLocalizedString(@"Email", nil);
    emailField.borderStyle = UITextBorderStyleRoundedRect;
    [emailField becomeFirstResponder];
    [alert addSubview:emailField];
    
    // add phone field
    UITextField *tf2 = [[UITextField alloc] initWithFrame:CGRectMake(16, 95, 252 , 28)];    
    
    self.phoneField = tf2;
    [tf2 release];
    
    phoneField.font = [UIFont systemFontOfSize:20];
    phoneField.backgroundColor = [UIColor whiteColor];
    phoneField.keyboardAppearance = UIKeyboardAppearanceAlert;
    phoneField.keyboardType = UIKeyboardTypePhonePad;
    phoneField.delegate = self;
    phoneField.placeholder = NSLocalizedString(@"Phone No.", nil);
    phoneField.borderStyle = UITextBorderStyleRoundedRect;
    [alert addSubview:phoneField];
    
    // set the tag for the registerDialog
}

-(void)dealloc {
    [alert release];
    [emailField release];
    [phoneField release];
    [super dealloc];
}

-(void)willPresentAlertView:(UIAlertView *)alertView {
    alert.frame = CGRectMake(alert.frame.origin.x, alert.frame.origin.y - 20, alert.frame.size.width, 200);
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kRegisterDialog) {
        if(buttonIndex == 1) {
            NSString *loginStr = [[alertView textFieldAtIndex:0] text];
            NSString *passStr = [[alertView textFieldAtIndex:1] text];

            if([loginStr isEqualToString:@""] || [passStr isEqualToString: @""]) {
                UIAlertView *aler = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"Please fill the email and phone fields.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                
                aler.tag = kFieldsFillError;
                [aler show];
                
                [aler release];
            }
            
            MRRegisterRequester *req = [[MRRegisterRequester alloc] initWithRequesterDeleagate:self];
        
            self.requester = req;
            [req release];
        
            [requester requestRegistrationWithEmail:loginStr andPhone:passStr];
        } 
    } else if(alertView.tag == kFieldsFillError) {
        [alert show];
    }
}

-(void)wsCallFinished:(BOOL)wasOk withResponse:(NSNumber *)responseCode {
    [requester release];
    
    // save the email and phone number for later use
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:emailField.text forKey:kRegisterEmail];
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:phoneField.text forKey:kRegisterPhone];
    
    [_delegate dialogFinishedTasks];
}

@end
