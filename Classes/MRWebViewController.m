//
//  ViewController.m
//  MobileREDContainer
//
//  Created by Dima Yarmoshuk on 9/20/15.
//  Copyright (c) 2015 SyncDog. All rights reserved.
//

#import "MRWebViewController.h"

@interface MRWebViewController ()
@property (nonatomic , strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation MRWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    NSURL *testUrl = [[NSURL alloc] initWithString:@"http://race/mobilered"];
    NSURL *testUrl = [[NSURL alloc] initWithString:@"http://onliner.by"];

    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:testUrl];
    
    [self.webView loadRequest:request];
    
//    GMMainMenuViewController  *vc = [[MobileRED sharedManager] mobileREDMainViewController];
//    if (vc)
//        [self.navigationController pushViewController:vc animated:YES];
    
    //[self presentViewController:vc animated:YES completion:nil];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
