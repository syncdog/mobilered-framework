//
//  MRLink.h
//  MobileRed
//
//  Created by Lion User on 06/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kIdUrlKey @"ID URL"
#define kNameKey @"NAME"
#define kUrlKey @"URL"

@interface MRLink : NSObject <NSCoding> {
    NSString *_idUrl;
    NSString *_name;
    NSString *_url;
}

@property (nonatomic, retain) NSString *idUrl;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *url;

-(id)initWithString:(NSString *)linkString;

@end
