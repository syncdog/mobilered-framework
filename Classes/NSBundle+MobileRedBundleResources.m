//
//  NSBundle+MobileRedBundleResources.m
//  MobileRed
//
//  Created by Dima Yarmoshuk on 9/20/15.
//
//

#import "NSBundle+MobileRedBundleResources.h"

@implementation NSBundle (MobileRedBundleResources)

+ (NSBundle*)mobileRedResourcesBundle {
    static dispatch_once_t onceToken;
    static NSBundle *myLibraryResourcesBundle = nil;
    dispatch_once(&onceToken, ^{
        myLibraryResourcesBundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]];
    });
    return myLibraryResourcesBundle;
}

@end
