//
//  MRUnregisterRequester.h
//  MobileRed
//
//  Created by Lion User on 09/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequester.h"

@interface MRUnregisterRequester : MRHttpRequester

-(void)unregisterDevice;

@end
