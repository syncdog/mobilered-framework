//
//  RegisterDialog.h
//  MobileRed
//
//  Created by Lion User on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequesterDelegate.h"
#import "MRDialogDelegate.h"

#define kRegisterDialog 1
#define kFieldsFillError 2

@class MRRegisterRequester;

@interface MRRegisterDialog : NSObject <UIAlertViewDelegate, UITextFieldDelegate, HttpRequesterDelegate> {
    id<MRDialogDelegate> _delegate;
}

@property (nonatomic, retain) UITextField *emailField, *phoneField;
@property (nonatomic, retain) UIAlertView *alert;
@property (nonatomic, retain) MRRegisterRequester *requester;

-(id) initDialogWithDelegate:(id<MRDialogDelegate>) delegate;
-(void) addFields;

@end
