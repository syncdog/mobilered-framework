//
//  MRAddressBook.m
//  MobileRed
//
//  Created by Lion User on 04/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRAddressBook.h"
#import "MRIndividualContact.h"
#import "MRGroupContact.h"

@implementation MRAddressBook 

@synthesize individualContacts = _individualContacts;
@synthesize groupContacts = _groupContacts;

-(id)init {
    self = [super init];
    
    if(self) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        self.individualContacts = array;
        [array release];
        
        NSMutableArray *array2 = [[NSMutableArray alloc] init];
        self.groupContacts = array2;
        [array2 release];
    }
    
    return self;
}

-(void)parseContacts:(NSString *)contacts {
    NSArray *newLineSplit = [contacts componentsSeparatedByString:@"\r\n"];
    
    for (NSString *item in newLineSplit) {
        if ([item hasPrefix:@"U"]) {
            MRIndividualContact *contact = [[MRIndividualContact alloc] init];
            [contact setContactAttributes:item];
            
            [_individualContacts addObject:contact];
            
            [contact release];
        } else if ([item hasPrefix:@"G"]) {
            MRGroupContact *contact = [[MRGroupContact alloc] init];
            [contact setGroupAttributes:item];
            
            [_groupContacts addObject:contact];
            
            [contact release];
        }
    }
    
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:self forKey:kAddressBook];
}

-(NSArray *)getContacts {
    NSMutableArray *allContacts = [[NSMutableArray alloc] init];
    
    if(_groupContacts != nil && [_groupContacts count] > 0) {
        for (MRGroupContact *gContact in _groupContacts) {
            [allContacts addObject:gContact];
        }
    }
    
    if (_individualContacts != nil && [_individualContacts count] > 0) {
        for (MRIndividualContact *iContact in _individualContacts) {
            [allContacts addObject:iContact];
        }
    }
    
    NSArray *toReturn = [NSArray arrayWithArray:allContacts];
    
    [allContacts release];
    
    return toReturn;
}

-(void)dealloc {
    [_individualContacts release];
    [_groupContacts release];
    
    [super dealloc];
}

#pragma NSCoding methods

- (id)initWithCoder:(NSCoder *)coder {
    self = [self init];
    
    if (self) {
        self.individualContacts = [coder decodeObjectForKey:kIndividiualContacts];
        self.groupContacts = [coder decodeObjectForKey:kGroupContacts];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_individualContacts forKey:kIndividiualContacts];
    [aCoder encodeObject:_groupContacts forKey:kGroupContacts];
}

@end
