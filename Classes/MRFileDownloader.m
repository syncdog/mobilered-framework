//
//  MRFileDownloader.m
//  MobileRed
//
//  Created by Lion User on 19/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRFileDownloader.h"
#import "MRDocument.h"
//#import "AFHTTPRequestOperation.h"

@implementation MRFileDownloader

@synthesize fileArray = _fileArray;

-(void)startDownloads:(NSArray *)filesToDownload {
    if([filesToDownload count] > 0) {
        [self retain];
        
        NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:filesToDownload];
        
        self.fileArray = arr;
        
        [arr release];
        
        [self downloadDocument:_fileArray];    
    }
}

-(void)downloadDocument:(NSMutableArray *)documentsArray {
    if ([documentsArray count] > 0) {
        MRDocument *doc = [documentsArray objectAtIndex:0];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:doc.downloadUrl]];
        
      //  AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:doc.fileName];
//        operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
//        
//        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//            [_fileArray removeObjectAtIndex:0];
//            [self downloadDocument:_fileArray];
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            [_fileArray removeObjectAtIndex:0];
//            [self downloadDocument:_fileArray];
//        }];
//        
//        [operation start];
//        
//        [operation release];
    } else {
        self.fileArray = nil;
        [self release];
    }
}

-(void)dealloc {
    [_fileArray release];

    [super dealloc];
}

@end
