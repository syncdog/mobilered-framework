//
//  HttpRequester.h
//  MobileRed
//
//  Created by Lion User on 23/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "MRHttpRequesterDelegate.h"
#import "HttpConstants.h"
#import "MobileRED.h"

@interface MRHttpRequester : NSObject <ASIHTTPRequestDelegate> {
    ASIFormDataRequest *_request;
    id<HttpRequesterDelegate> _requester;
    NSInteger _requestIdentifier;
}

@property (nonatomic, retain) ASIFormDataRequest *request;

-(id)initWithRequesterDeleagate:(id<HttpRequesterDelegate>) controller;
-(void) setInitialParams;
-(void) setRefreshParameter;
-(void) processResponse:(NSData *)data; 

@end
