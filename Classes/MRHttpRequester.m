//
//  HttpRequester.m
//  MobileRed
//
//  Created by Lion User on 23/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRHttpRequester.h"

#import "MRHttpRequesterDelegate.h"

@implementation MRHttpRequester

@synthesize request = _request;

-(id)initWithRequesterDeleagate:(id<HttpRequesterDelegate>)controller {
    self = [super init];
    
    if(self != nil) {
        _requester = controller;
    }
    
    return self;
}

-(void)setInitialParams {
    // set IMEI
    UIDevice *imeiHelper =[UIDevice currentDevice];
    NSString *imei = imeiHelper.identifierForVendor.UUIDString;
    
    [_request setPostValue:imei forKey:kParameterIMEI];
    
    // set OS identifier
    [_request setPostValue:kiOSIdentifier forKey:kParameterOS];
    
    // set the app version
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [_request setPostValue:version forKey:kParameterAppVersion];
}

-(void)requestSettings {
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    self.request = [ASIFormDataRequest requestWithURL:url];
    [self setInitialParams];
    [self setRefreshParameter];
    
    // set action
    [_request setPostValue:kActionSettings forKey:kParameterAction];
    
    _request.delegate = self;
    [_request startAsynchronous];
}

-(void)setRefreshParameter {
    // set refresh flag
    [_request setPostValue:@"1" forKey:kParameterRefresh];
}

#pragma ASIDelegateMethods

-(void)requestFinished:(ASIHTTPRequest *)requested {
    NSNumber *responseCode = [NSNumber numberWithInt:[requested responseStatusCode]];
    
    NSData *data = [requested responseData];
    
    [self processResponse:data];
    if([_requester respondsToSelector:@selector(wsCallFinished:withResponse:forRequest:)]) {
        [_requester wsCallFinished:TRUE withResponse:responseCode forRequest:_requestIdentifier];
    } else {
        [_requester wsCallFinished:TRUE withResponse:responseCode];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request {
    if([_requester respondsToSelector:@selector(wsCallFinished:withResponse:forRequest:)]) {
        [_requester wsCallFinished:FALSE withResponse:[NSNumber numberWithInt:[request responseStatusCode]] forRequest:_requestIdentifier];
    } else {
        [_requester wsCallFinished:FALSE withResponse:[NSNumber numberWithInt:[request responseStatusCode]]];    
    }
}

-(void)processResponse:(NSData *)data {
    // implement this method in subclasses to handle specific responses
}

@end
