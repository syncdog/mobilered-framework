//
//  MRContactDetailsViewController.m
//  MobileRed
//
//  Created by Lion User on 24/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRContactDetailsViewController.h"
#import "MRContact.h"
#import "MRIndividualContact.h"
#import "MRGroupContact.h"

@implementation MRContactDetailsViewController

@synthesize nameLabel = _nameLabel;
@synthesize nameValue = _nameValue;
@synthesize emailLabel = _emailLabel;
@synthesize emailValue = _emailValue;
@synthesize phoneLabel = _phoneLabel;
@synthesize phoneValue = _phoneValue;

@synthesize callButton = _callButton;
@synthesize mredButton = _mredButton;
@synthesize sendEmail = _sendEmail;
@synthesize sendSMSButton = _sendSMSButton;

@synthesize contact = _contact;

-(id)initWithContact:(MRContact *)thecontact {
    self = [super initWithNibName:@"MRContactDetailsViewController" bundle:nil];
    
    if (self) {
        self.contact = thecontact;
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc {
    [_nameLabel release];
    [_nameValue release];
    [_emailLabel release];
    [_emailValue release];
    [_phoneLabel release];
    [_phoneValue release];
    [_callButton release];
    [_mredButton release];
    [_sendEmail release];
    [_sendSMSButton release];
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Details", nil);
    
    _nameLabel.text = NSLocalizedString(@"Name", nil);
    _nameValue.text = _contact.name;
    _emailLabel.text = NSLocalizedString(@"Email", nil);
    _emailValue.text = @"";
    _phoneLabel.text = NSLocalizedString(@"Phone", nil);
    _phoneValue.text = @"";
    
    _callButton.titleLabel.text = NSLocalizedString(@"Call", nil);
    _mredButton.titleLabel.text = NSLocalizedString(@"Send MRED Mess", nil);
    _sendEmail.titleLabel.text = NSLocalizedString(@"Send Email", nil);
    _sendSMSButton.titleLabel.text = NSLocalizedString(@"Send SMS", nil);
}

- (void)viewDidUnload {
    _nameLabel = nil;
    _nameValue = nil;
    _emailLabel = nil;
    _emailValue = nil;
    _phoneLabel = nil;
    _phoneValue = nil;
    _callButton = nil;
    _mredButton = nil;
    _sendEmail = nil;
    _sendSMSButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
