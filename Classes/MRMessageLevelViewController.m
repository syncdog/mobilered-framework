//
//  MRMessageLevelViewController.m
//  MobileRed
//
//  Created by Lion User on 26/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRMessageLevelViewController.h"
#import "MRMREDMessage.h"
#import "MRTemplatePickerViewController.h"

@implementation MRMessageLevelViewController

@synthesize message =_message;
@synthesize emergencyButton = _emergencyButton;
@synthesize warningButton = _warningButton;
@synthesize informationButton = _informationButton;

-(id)initWithMessageType:(MRMREDMessage *)theMessage {
    self = [super initWithNibName:@"MRMessageLevelViewController" bundle:[NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]]];
    
    if (self) {
        self.message = theMessage;
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)buttonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    switch (button.tag) {
        case kEmergencyButtonTag:
            _message.level = levelEmergency;
            break;
        case kWarningButtonTag:
            _message.level = levelWarning;
            break;
        case kInformationButtonTag:
            _message.level = levelInformation;
            break;
        default:
            break;
    }
    
    MRTemplatePickerViewController *controller = [[[MRTemplatePickerViewController alloc] initWithMessage:_message] autorelease];
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)dealloc {
    [_message release];
    [_emergencyButton release];
    [_warningButton release];
    [_informationButton release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _message.hasLink = false;
    _message.hasPhone = false;
    _message.templateT = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Message Level", nil);
    
    _emergencyButton.titleLabel.text = NSLocalizedString(@"Emergency", nil);
    _warningButton.titleLabel.text = NSLocalizedString(@"Warning", nil);
    _informationButton.titleLabel.text = NSLocalizedString(@"Information", nil);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.warningButton = nil;
    self.emergencyButton = nil;
    self.informationButton = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
