//
//  MRMessageSender.h
//  MobileRed
//
//  Created by Marius Gherman on 5/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MRMREDMessage;
@class MRIndividualContact;

@interface MRMessageSender : NSObject {
    MRMREDMessage *_message;
}

@property (nonatomic, retain) MRMREDMessage *message;

-(id)initWithMessage:(MRMREDMessage *)theMessage;
-(void)sendMessage;
-(MRIndividualContact *)findMe;
-(NSString *)getRecepients;

@end
