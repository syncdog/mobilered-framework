//
//  MRContact.m
//  MobileRed
//
//  Created by Lion User on 04/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRContact.h"

@implementation MRContact

@synthesize idContact = _idContact;
@synthesize name = _name;
@synthesize cecked = _cecked;

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_idContact forKey:kID];
    [aCoder encodeObject:_name forKey:kName];
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    if(self) {
        self.idContact = [aDecoder decodeObjectForKey:kID];
        self.name = [aDecoder decodeObjectForKey:kName];
    }
    return self;
}

-(void)dealloc {
    [_idContact release];
    [_name release];
    
    [super dealloc];
}

@end
