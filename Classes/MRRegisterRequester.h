//
//  MRRegisterRequester.h
//  MobileRed
//
//  Created by Lion User on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequester.h"

@interface MRRegisterRequester : MRHttpRequester

-(void) requestRegistrationWithEmail:(NSString *)email andPhone:(NSString *)phone;

@end
