//
//  MRAddressBookRequester.m
//  MobileRed
//
//  Created by Lion User on 03/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRAddressBookRequester.h"
#import "MRAddressBook.h"

@implementation MRAddressBookRequester

-(void)requestAddressBook {
    _requestIdentifier = kAddressBookRequest;
    
    NSURL *url = [NSURL URLWithString:[MRRuntimeStoreAccessor getFromRuntimeStore:kURL]];
    
    self.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [_request setPostValue:kActionAddressBook forKey:kParameterAction];
    
    _request.delegate = self;
    [_request startAsynchronous];
}

-(void)processResponse:(NSData *)data {
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    MRAddressBook *book = [[MRAddressBook alloc] init];
    [book parseContacts:response];
    
    [book release];
    [response release];
}

@end
