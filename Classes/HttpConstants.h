//
//  HttpConstants.h
//  MobileRed
//
//  Created by Lion User on 23/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define kBaseUrl @"http://home.gyllensvaan.com/mobilered/submitdata.asp"

//---------------------------

#pragma Platform constants

#define kiOSIdentifier @"1"

//-------------------------------

#pragma Http General Parameters

#define kParameterAction @"ACT"
#define kParameterAppVersion @"APPVERSION"
#define kParameterIMEI @"IMEI"
#define kParameterRefresh @"REFRESH"
#define kParameterOS @"OS"
#define kParameterFormId @"id"

//-------------------------------

#pragma Actions

#define kActionSettings @"getsettings"
#define kActionRegister @"register"
#define kActionAddressBook @"getaddress"
#define kActionLinks @"getlinks"
#define kActionTemplate @"gettemplates"
#define kActionCC @"getcc"
#define kActionDocuments @"getDocuments"
#define kActionUnregister @"unregister"
#define kActionGetForms @"getForms"
#define kActionGetForm @"getForm"
#define kActionApproveMessage @"approvemessage"
#define kActionPushMessage @"pushmessage"

//--------------------------------

#pragma Message push

#define kMessagePushFrom @"FROM"
#define kMessagePushName @"NAME"
#define kMessagePushSubject @"SUBJECT"
#define kMessagePushMessage @"MESSAGE"
#define kMessagePushMessageId @"MSGID"
#define kMessagePushVoice @"VOICE"
#define kMessagePushLevel @"LEVEL"
#define kMessagePushLink @"LINK"
#define kMessagePushPhone @"PHONE"
#define kMessagePushTo @"TO"

//--------------------------------

#pragma Request identifiers

#define kSettingsRequest 1000
#define kAddressBookRequest 1001
#define kLinksRequest 1002
#define kTemplateRequest 1003
#define kCCRequest 1004
#define kDocumentsRequest 1005
#define kUnregisterRequest 1006
#define kFormsRequest 1007