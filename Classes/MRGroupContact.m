//
//  MRGroupContact.m
//  MobileRed
//
//  Created by Lion User on 04/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRGroupContact.h"

@implementation MRGroupContact 

@synthesize groupIds = _groupIds;
@synthesize approveId = _approveId;

-(id)init {
    self = [super init];
    
    if(self) {
        [self initialize];
    }
    
    return self;
}

-(void)initialize {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    self.groupIds = arr;
    
    [arr release];
}

-(void)setGroupAttributes:(NSString *)groupString {
    NSArray *array = [groupString componentsSeparatedByString:@"|"];
    
    self.idContact = [array objectAtIndex:1];
    self.name = [array objectAtIndex:3];
    self.groupIds = [[array objectAtIndex:4] componentsSeparatedByString:@","];
    self.approveId = [array objectAtIndex:5];
}

-(void)dealloc {
    [_groupIds release];
    [_approveId release];
    
    [super dealloc];
}

#pragma NSCoding

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self initialize];
        
        self.groupIds =[aDecoder decodeObjectForKey:kGroupIds];
        self.approveId = [aDecoder decodeObjectForKey:kApproveId];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_groupIds forKey:kGroupIds];
    [aCoder encodeObject:_approveId forKey:kApproveId];
}

@end
