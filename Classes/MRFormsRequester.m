//
//  MRFormsRequester.m
//  MobileRed
//
//  Created by Lion User on 09/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRFormsRequester.h"
#import "MRFormsXMLRequester.h"

@implementation MRFormsRequester 

@synthesize formIds = _formIds;
@synthesize requ = _requ;

-(void)requestForms {
    _requestIdentifier = kFormsRequest;
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    self.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [_request setPostValue:kActionGetForms forKey:kParameterAction];
    
    self.request.delegate = self;
    [_request startAsynchronous];
}

-(void)processResponse:(NSData *)data {
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *str;
    if([response hasSuffix:@"\r\n"]) {
        str = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else {
        str = response;
    }
    
    NSMutableArray *newLineSplit = [NSMutableArray arrayWithArray:[str componentsSeparatedByString:@","]];
    
    if(! [[newLineSplit objectAtIndex:0] isEqualToString:@""]) {
        self.formIds = newLineSplit;
        
        NSString *firstId = [newLineSplit objectAtIndex:0];
        
        [self getForm:firstId];
        
    } else {
        [self notifyRequesterWithResponseCode:[NSNumber numberWithInt:200]];
    }
    
    [response release];
}

-(void)getForm:(NSString *)formId {
    MRFormsXMLRequester *req = [[MRFormsXMLRequester alloc] initWithRequesterDeleagate:self];
    
    self.requ = req;
    
    [req release];
    
    [_requ getFormXML:formId];
}

-(void)notifyRequesterWithResponseCode:(NSNumber *)responseCode {
    if([_requester respondsToSelector:@selector(wsCallFinished:withResponse:forRequest:)]) {
        [_requester wsCallFinished:TRUE withResponse:responseCode forRequest:_requestIdentifier];
    } else {
        [_requester wsCallFinished:TRUE withResponse:responseCode];
    }
}

#pragma ASIHttpDelegate methods

-(void)requestFinished:(ASIHTTPRequest *)request {
    NSData *data = [request responseData];
    
    [self processResponse:data];
}

#pragma MRHttpRequesterDelegate methods

-(void)wsCallFinished:(BOOL)wasOk withResponse:(NSNumber *)responseCode {
    [_formIds removeObjectAtIndex:0];
    
    if ([_formIds count] > 0) {
        [self getForm:[_formIds objectAtIndex:0]];
    } else {
        [_requ release];
        
        [self notifyRequesterWithResponseCode:responseCode];
    }
}

@end
