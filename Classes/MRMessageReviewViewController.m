//
//  MRMessageReviewViewController.m
//  MobileRed
//
//  Created by Marius Gherman on 5/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRMessageReviewViewController.h"
#import "MRMREDMessage.h"

#import "MRMessageTypeViewController.h"
#import "MRFinalStepViewController.h"
#import "MRURLPickerViewController.h"
#import "MRPhonePicvkerViewController.h"
#import "MRContactsViewController.h"
#import "MRAddressBook.h"
#import "MRContact.h"

@implementation MRMessageReviewViewController

@synthesize message = _message;
@synthesize toLabel = _toLabel, recepientsLabel = _recepientsLabel, sendCCLabel = _sendCCLabel, deliverVoiceLabel = _deliverVoiceLabel, messageLevelLabel = _messageLevelLabel, messageLevelValueLabel = _messageLevelValueLabel, deliveryLabel = _deliveryLabel, deliveryValueLabel = _deliveryValueLabel, attachmentsLabel = _attachmentsLabel;
@synthesize recepientsButton = _recepientsButton, linkButton = _linkButton, ccButton = _ccButton;
@synthesize ccSwitch = _ccSwitch, deliverVoiceSwitch = _deliverVoiceSwitch;
@synthesize levelSlider = _levelSlider, deliverySlider = _deliverySlider;

-(void)changeUiToMatchMessageSettings {
    [self changeLevel:_message.level changeSlider:YES];
    [self changeDelivery:_message.type changeSlider:YES];
    
    if (_message.link == nil) {
        _linkButton.enabled = false;
    }
    
    if (_message.cc == nil) {
        _ccButton.enabled = false;
    }
}

-(void)sliderChanged:(id)sender {
    UISlider *slider = (UISlider *) sender;
    
    NSInteger intValue;
    switch (slider.tag) {
        case kLevelSliderTag:
            if (slider.value <= 1.5) {
                intValue = 1;
            } else if (slider.value > 1.5 && slider.value <= 2.5 ) {
                intValue = 2;
            } else {
                intValue = 3;
            }
            
            _message.level = intValue;
            [self changeLevel:intValue changeSlider:YES];
            break;
        case kDeliverySliderTag:
            if (slider.value <= 0.5) {
                intValue = 0;
            } else if (slider.value > 0.5 && slider.value <= 1.5 ) {
                intValue = 1;
            } else {
                intValue = 2;
            }
            
            _message.type = intValue;
            [self changeDelivery:intValue changeSlider:YES];
            break;
        default:
            break;
    }
}

-(void)changeLevel:(NSInteger)level changeSlider:(BOOL)change{
    NSString *str = nil;
    
    switch (level) {
        case levelEmergency:
            str = NSLocalizedString(@"Emergency", nil);
            break;
        case levelWarning:
            str = NSLocalizedString(@"Warning", nil);
            break;
        case levelInformation:
            str = NSLocalizedString(@"Information", nil);
            break;
        default:
            break;
    }
    
    _messageLevelValueLabel.text = str; 
    if (change) {
        [_levelSlider setValue:level animated:NO];
    }
}

-(void)changeDelivery:(NSInteger)level changeSlider:(BOOL)change {
    NSString *str = nil;
    
    switch (level) {
        case typePushMessage:
            str = NSLocalizedString(@"Push", nil);
            break;
        case typeSMS:
            str = NSLocalizedString(@"SMS", nil);
            break;
        case typeAnyAvailable:
            str = NSLocalizedString(@"Any available", nil);
            break;
        default:
            break;
    }
    
    _deliveryValueLabel.text = str;
    if (change) {
        [_deliverySlider setValue:level animated:NO];
    }
}

-(void)buttonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    UIViewController *ctrl = nil;
    switch (button.tag) {
        case kLinkButtonTag:
            ctrl = [[[MRURLPickerViewController alloc] initWithMessage:_message andNextButton:NO] autorelease];
            break;
        case kCCButtonTag:
            ctrl = [[[MRPhonePicvkerViewController alloc] initWithMessage:_message andHasNext:NO] autorelease];
            break;
        case kSelectRecepientsButtonTag:
            ctrl = [[[MRContactsViewController alloc] initWithStyle:UITableViewStylePlain isCheckable:YES] autorelease];
            break;
        default:
            break;
    }
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

#pragma mark - View Lifecycle methods

-(id)initWithMessage:(MRMREDMessage *)theMessage {
    self = [super initWithNibName:@"MRMessageReviewViewController" bundle:[NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]]];
    
    if (self) {
        self.message = theMessage;
    }
    
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Finish - Step 1", nil);
    
    _toLabel.text = NSLocalizedString(@"To : ", nil);
    _recepientsLabel.text = NSLocalizedString(@"<Recepients>", nil);
    _sendCCLabel.text = NSLocalizedString(@"Send CC Email", nil);
    _deliverVoiceLabel.text = NSLocalizedString( @"Deliver Voice Message", nil);
    _messageLevelLabel.text = NSLocalizedString(@"Message Level", nil);
    _messageLevelValueLabel.text = NSLocalizedString(@"Level", nil);
    _deliveryLabel.text = NSLocalizedString(@"Delivery", nil);
    _deliveryValueLabel.text = NSLocalizedString(@"Delivery", nil);
    _attachmentsLabel.text = NSLocalizedString(@"Attachment(s)", nil);
    
   // _recepientsButton.titleLabel.text = NSLocalizedString(@"Select recepients", nil);
    _linkButton.titleLabel.text = NSLocalizedString(@"Link", nil);
    _ccButton.titleLabel.text = NSLocalizedString(@"CC", nil);
    
    UIBarButtonItem *item =[[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Finish", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(nextStep)] autorelease];
    
    [self.navigationItem setRightBarButtonItem:item animated:YES];
    
    UIButton *backButton = [UIButton buttonWithType:101];
    [backButton addTarget:self action:@selector(backStep) forControlEvents:UIControlEventTouchUpInside];
    [backButton setTitle:NSLocalizedString(@"Template", nil) forState:UIControlStateNormal];
    
    UIBarButtonItem *item1 = [[[UIBarButtonItem alloc] initWithCustomView:backButton] autorelease];
    
    [self.navigationItem setLeftBarButtonItem:item1 animated:YES];
    
    [self changeUiToMatchMessageSettings];
}

-(void)viewDidAppear:(BOOL)animated {
    MRAddressBook *book = [MRRuntimeStoreAccessor getFromRuntimeStore:kAddressBook];
    
    NSMutableString *str = [[NSMutableString alloc] init];
    
    for (MRContact *ctc in book.individualContacts) {
        if (ctc.cecked) {
            [str appendString:ctc.name];
            [str appendString:@", "];
        }
    }
    
    for (MRContact *ctc in book.groupContacts) {
        if (ctc.cecked) {
            [str appendString:ctc.name];
            [str appendString:@", "];
        }
    }
    
    if (! [str isEqualToString:@""]) {
         _recepientsLabel.text = str;
    }
    
    [str release];
}

-(void)backStep {
    MRAddressBook *book = [MRRuntimeStoreAccessor getFromRuntimeStore:kAddressBook];
    
    for (MRContact *ctc in book.individualContacts) {
        ctc.cecked = FALSE;
    }
    
    for (MRContact *ctc in book.groupContacts) {
        ctc.cecked = FALSE;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)nextStep {
    if (_deliverVoiceSwitch.isOn) {
        _message.includeVoice = TRUE;
    }
    
    if (_ccSwitch.isOn) {
        _message.sendCC = TRUE;
    }
    
    MRFinalStepViewController *ctrl = [[[MRFinalStepViewController alloc] initWithMessage:_message] autorelease];
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.toLabel = nil;
    self.recepientsLabel = nil;
    self.sendCCLabel = nil;
    self.deliverVoiceLabel = nil;
    self.messageLevelLabel = nil;
    self.messageLevelValueLabel = nil;
    self.deliveryLabel = nil;
    self.deliveryValueLabel = nil;
    self. attachmentsLabel = nil;
    
    self.recepientsButton = nil;
    self.linkButton = nil;
    self.ccButton = nil;
    
    self.ccSwitch = nil;
    self.deliverVoiceSwitch = nil;
    
    self.levelSlider = nil;
    self.deliverySlider = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc {
    [_message release];

    [_toLabel release];
    [_recepientsLabel release];
    [_sendCCLabel release];
    [_deliverVoiceLabel release];
    [_messageLevelLabel release];
    [_messageLevelValueLabel release];
    [_deliveryLabel release];
    [_deliveryValueLabel release];
    [_attachmentsLabel release];
    
    [_recepientsButton release];
    [_linkButton release];
    [_ccButton release];
    
    [_ccSwitch release];
    [_deliverVoiceSwitch release];
    
    [_levelSlider release];
    [_deliverySlider release];
    
    [super dealloc];
}

@end
