//
//  MRLinksRequester.m
//  MobileRed
//
//  Created by Lion User on 05/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRLinksRequester.h"
#import "MRLink.h"

@implementation MRLinksRequester

-(void)requestLinks {
    _requestIdentifier = kLinksRequest;
    
    NSURL *url = [NSURL URLWithString:[MRRuntimeStoreAccessor getFromRuntimeStore:kURL]];
    
    super.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [super.request setPostValue:kActionLinks forKey:kParameterAction];
    
    super.request.delegate = self;
    [super.request startAsynchronous];
}

-(void)processResponse:(NSData *)data {
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *str = nil;
    if([response hasSuffix:@"\r\n"]) {
        str = [response substringToIndex:[response rangeOfString:@"\r\n" options:NSBackwardsSearch].location];
    } else {
        str = response;
    }
    
    NSMutableArray *linksArray = [[NSMutableArray alloc] init];
    
    NSArray *newLineSplit = [str componentsSeparatedByString:@"\r\n"];
    
    for (NSString *item in newLineSplit) {
        MRLink *link =[[MRLink alloc] initWithString:item];
        
        [linksArray addObject:link];
        
        [link release];
    }
    
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:linksArray forKey:kLinks];
    
    [linksArray release];
    [response release];
}

@end
