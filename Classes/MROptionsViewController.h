//
//  MROptionsViewController.h
//  MobileRed
//
//  Created by Lion User on 02/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MRHttpRequesterDelegate.h"

#define kUnregisterAlertTag 1
#define kConfigurationAlertTag 2
#define kErrorAlertTag 3

@class MRHttpRequester;

@interface MROptionsViewController : UIViewController <HttpRequesterDelegate, UIAlertViewDelegate, UITextFieldDelegate> {
    MRHttpRequester *_requester;
    UIAlertView *_progress;
    UITextField *_configPassword;
    UIButton *_refresh;
}

@property (nonatomic, retain) UITextField *configPassword;
@property (nonatomic, retain) MRHttpRequester *requester;
@property (nonatomic, retain) UIAlertView *progress;
@property (nonatomic, retain) IBOutlet UIButton *refresh;

- (id) initFromBundle;

-(IBAction)configureSettings:(id)sender;
-(IBAction)deviceInfo:(id)sender;
-(IBAction)refreshDevice:(id)sender;
-(IBAction)unregisterDevice:(id)sender;
-(IBAction)documentBrowser:(id)sender;

-(void)getSettings;
-(void)getAddressBook;
-(void)getLinks;
-(void)getTemplates;
-(void)getCC;
-(void)getDocuments;
-(void)getForms;

-(void)unregisterCleanup;

#pragma ProgressAlertMethods

-(void)showProgressAlertWithMessage:(NSString *)message;
-(void)closeProgress;

@end
