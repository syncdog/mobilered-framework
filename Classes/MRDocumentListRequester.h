//
//  MRDocumentsRequester.h
//  MobileRed
//
//  Created by Marius Gherman on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequester.h"

@interface MRDocumentListRequester : MRHttpRequester

-(void)requestDocuments;
-(NSArray *)compareDocuments:(NSMutableArray *)array;

@end
