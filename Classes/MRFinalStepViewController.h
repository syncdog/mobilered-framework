//
//  MRFinalStepViewController.h
//  MobileRed
//
//  Created by Marius Gherman on 5/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MRMREDMessage;

@interface MRFinalStepViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate> {
    MRMREDMessage *_message;
    UILabel *_subjectLabel, *_messageLabel;
    UITextField *_subjectText;
    UITextView *_messageText;
}

@property (nonatomic, retain) MRMREDMessage *message;
@property (nonatomic, retain) IBOutlet UILabel *subjectLabel, *messageLabel;
@property (nonatomic, retain) IBOutlet UITextField *subjectText; 
@property (nonatomic, retain) IBOutlet UITextView *messageText;
-(id)initWithMessage:(MRMREDMessage *)theMessage;

@end
