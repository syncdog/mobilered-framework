//
//  GMMainMenuViewController.m
//  MobileRed
//
//  Created by Lion User on 19/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GMMainMenuViewController.h"

#import "ASIHTTPRequest.h"
#import "ASIHTTPRequestConfig.h"

#import "MRRegisterDialog.h"
#import "MRSettingsRequester.h"
#import "MobileRED.h"
#import "MROptionsViewController.h"
#import "MRContactsViewController.h"
#import "MRMessageTypeViewController.h"
#import "NSBundle+MobileRedBundleResources.h"
#import "RuntimePersistenceHelper.h"

@implementation GMMainMenuViewController

@synthesize createMessages = _createMessages;
@synthesize receivedMessages = _receivedMessages;
@synthesize constacts = _constacts;
@synthesize sentMessages = _sentMessages;
@synthesize forms = _forms;
@synthesize options = _options;

@synthesize requester = _requester;
@synthesize registerDialog = _registerDialog;
@synthesize progress = _progress;

- (id)initMainViewController {
    
    if ((self = [super initWithNibName:@"MainMenu" bundle:[NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]]])) {
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        [MobileRED sharedManager].helper = [RuntimePersistenceHelper sharedInstance];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc {
    [_createMessages release];
    [_receivedMessages release];
    [_constacts release];
    [_sentMessages release];
    [_forms release];
    [_options release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    // set the title for this view
    self.title = @"mobileRED";
   
    // set the localized names of the button labels
    _createMessages.text = NSLocalizedString(@"Create Messages", nil);
    _receivedMessages.text = NSLocalizedString(@"received_messages", nil);
    _constacts.text = NSLocalizedString(@"contacts", nil);
    _sentMessages.text = NSLocalizedString(@"sent_messages", nil);
    _forms.text = NSLocalizedString(@"forms", nil);
    _options.text = NSLocalizedString(@"options", nil);
    
    
    if(! [[MobileRED sharedManager].runtimeStore objectForKey:kRegisteredKey]) {
        [self showRegisterDialog];
    } else {
        if ([CheckInternetAvailability hasConnectivity]) {
            [[MobileRED sharedManager].runtimeStore setValue:[NSNumber numberWithBool:NO] forKey:kOfflineModeKey];
            
            [self getSettings];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"No internet connection available. Would you like to continue in offline mode ?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"No", nil) otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
            
            [alert show];
            
            [alert release];
        }
    }
}

- (void)viewDidUnload
{
    self.createMessages = nil;
    self.receivedMessages = nil;
    self.constacts = nil;
    self.sentMessages = nil;
    self.forms = nil;
    self.options = nil;
    self.requester = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma Self methods

-(void)getSettings {
    [self showProgressAlertWithMessage:NSLocalizedString(@"Getting settings", nil)];
    
    MRSettingsRequester *rq = [[MRSettingsRequester alloc] initWithRequesterDeleagate:self];
    
    self.requester = rq;
    
    [rq release];
    
    [_requester requestSettings];
}

-(void)wsCallFinished:(BOOL)wasOk withResponse:(NSNumber *)response{
    [_requester release];
    
    [self closeProgress];
}

-(void)showRegisterDialog {
    MRRegisterDialog *di = [[MRRegisterDialog alloc] initDialogWithDelegate:self];
    
    self.registerDialog = di;
    
    [di release];
    
    [_registerDialog.alert show];
}

-(void)dialogFinishedTasks {
    [_registerDialog release];
    
    [self getSettings];
}

#pragma UIAlertDialogDelegate methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        exit(1);
    } else if (buttonIndex == 1) {
        [MRRuntimeStoreAccessor setInRuntimeStoreObject:[NSNumber numberWithBool:YES] forKey:kOfflineModeKey];
    }
}

#pragma Button actions

-(void)buttonAction:(id)sender {
    UIViewController *controller = nil;
    
    UIButton *buttonPressed = sender;
    
    switch (buttonPressed.tag) {
        case kOptionsButtonTag:
//            controller = [[MROptionsViewController alloc] initWithNibName:@"MROptionsViewController" bundle:nil];
            controller = [[MROptionsViewController alloc] initFromBundle];

            break;
        case kContactsButtonTag:
            controller = [[MRContactsViewController alloc] initWithStyle:UITableViewStylePlain];
            break;
        case kCreateMessageButtonTag:
            controller = [[MRMessageTypeViewController alloc] initWithNibName:@"MRMessageTypeViewController" bundle:[NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]]];
            break;
        default:
            break;
    }
    
    [[self navigationController] pushViewController:controller animated:YES];
    
    [controller release];
}

#pragma Progress alert view methods

-(void)showProgressAlertWithMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:NSLocalizedString(@"Please wait...", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activity.frame = CGRectMake(139.0f - 18.0f, 80.0f, 37.0f, 37.0f);
    [alert addSubview:activity];
    [activity startAnimating];
    
    self.progress = alert;
    [alert release];
    [activity release];
    
    [_progress show];
}

-(void)closeProgress {
    [_progress dismissWithClickedButtonIndex:0 animated:YES];
    
    [_progress release];
}

@end
