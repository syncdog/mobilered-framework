//
//  MRUnregisterRequester.m
//  MobileRed
//
//  Created by Lion User on 09/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRUnregisterRequester.h"

@implementation MRUnregisterRequester

-(void)unregisterDevice {
    _requestIdentifier = kUnregisterRequest;
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    self.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [_request setPostValue:kActionUnregister forKey:kParameterAction];
    
    self.request.delegate = self;
    [_request startAsynchronous];
}

@end
