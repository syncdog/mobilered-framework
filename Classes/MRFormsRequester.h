//
//  MRFormsRequester.h
//  MobileRed
//
//  Created by Lion User on 09/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHttpRequester.h"
#import "MRHttpRequesterDelegate.h"

@class MRFormsXMLRequester;

@interface MRFormsRequester : MRHttpRequester <HttpRequesterDelegate, ASIHTTPRequestDelegate> {
    NSMutableArray *_formIds;
    MRFormsXMLRequester *_requ;
}

@property (nonatomic, retain) NSMutableArray *formIds;
@property (nonatomic, retain) MRFormsXMLRequester *requ;

-(void)requestForms;
-(void)notifyRequesterWithResponseCode:(NSNumber *)responseCode;

-(void)getForm:(NSString *)formId;

@end
