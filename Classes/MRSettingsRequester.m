//
//  MRSettingsRequester.m
//  MobileRed
//
//  Created by Lion User on 02/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MRSettingsRequester.h"

@implementation MRSettingsRequester

-(void)requestSettings {
    _requestIdentifier = kSettingsRequest;
    
    NSURL *url = [NSURL URLWithString:kBaseUrl];
    
    super.request = [ASIFormDataRequest requestWithURL:url];
    [super setInitialParams];
    
    [super.request setPostValue:kActionSettings forKey:kParameterAction];
    
    super.request.delegate = self;
    [super.request startAsynchronous];
}

-(void)processResponse:(NSData *)data {
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
 
    NSLog(@"%@",dataString);
    
    NSArray *paramsArray = [dataString componentsSeparatedByString:@"|"];
    
    [dataString release];
    
    NSString *url = [paramsArray objectAtIndex:0];
    NSString *refreshTime = [paramsArray objectAtIndex:2];
    NSString *settingsPassword = [paramsArray objectAtIndex:3];
    NSString *levels = [[NSString alloc] initWithFormat:@"%@|%@|%@", [paramsArray objectAtIndex:4], [paramsArray objectAtIndex:5], [paramsArray objectAtIndex:6]];
    
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:url forKey:kURL];
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:refreshTime forKey:kRefreshTime];
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:settingsPassword forKey:kSettingsPassword];
    [MRRuntimeStoreAccessor setInRuntimeStoreObject:levels forKey:kMessagesLevels];
    
    [levels release];
}

@end
