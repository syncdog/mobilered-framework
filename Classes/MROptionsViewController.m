//
//  MROptionsViewController.m
//  MobileRed
//
//  Created by Lion User on 02/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MROptionsViewController.h"
#import "MRSettingsRequester.h"
#import "MRAddressBookRequester.h"
#import "MRLinksRequester.h"
#import "MRTemplateRequester.h"
#import "MRConferenceCallRequester.h"
#import "MRDocumentListRequester.h"
#import "MRUnregisterRequester.h"
#import "MRFormsRequester.h"
#import "MRConfigureSettingsViewController.h"

@implementation MROptionsViewController

@synthesize configPassword = _configPassword;
@synthesize requester = _requester;
@synthesize progress = _progress;
@synthesize refresh = _refresh;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initFromBundle {
    NSString *xibName = @"MROptionsViewController";
    NSBundle *bundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"MobileRED" withExtension:@"bundle"]];
    self = [super initWithNibName:xibName bundle:bundle];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc {
    [_progress release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Options", nil);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma Button actions

-(void)configureSettings:(id)sender {
    UIAlertView *configPasswordAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Enter password", nil) message:@"\n" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    
    configPasswordAlert.tag = kConfigurationAlertTag;
    configPasswordAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
   // UITextField *tf1 = [[UITextField alloc] initWithFrame:CGRectMake(16, 45, 252 , 28)];
    
    self.configPassword = [configPasswordAlert textFieldAtIndex:0];
   // [tf1 release];
    
    _configPassword.font = [UIFont systemFontOfSize:20];
    _configPassword.backgroundColor = [UIColor whiteColor];
    _configPassword.keyboardAppearance = UIKeyboardAppearanceAlert;
    _configPassword.keyboardType = UIKeyboardTypeEmailAddress;
    _configPassword.delegate = self;
    _configPassword.placeholder = NSLocalizedString(@"Password", nil);
    _configPassword.borderStyle = UITextBorderStyleRoundedRect;
    _configPassword.secureTextEntry = YES;
    [_configPassword becomeFirstResponder];
    //[configPasswordAlert addSubview:_configPassword];
    
    [configPasswordAlert show];
    [configPasswordAlert release];
}

-(void)refreshDevice:(id)sender {
    [self getSettings];
}

-(void)unregisterDevice:(id)sender {
    [self showProgressAlertWithMessage:NSLocalizedString(@"Getting settings", nil)];
    
    MRUnregisterRequester *req = [[MRUnregisterRequester alloc] initWithRequesterDeleagate:self];
    
    self.requester = req;
    
    [req release];
    
    [(MRUnregisterRequester *)_requester unregisterDevice];
}

#pragma requests

-(void)getSettings {
    [self showProgressAlertWithMessage:NSLocalizedString(@"Getting settings", nil)];
    
    MRSettingsRequester *req = [[MRSettingsRequester alloc] initWithRequesterDeleagate:self];
    
    self.requester = req;
    
    [req release];
    
    [(MRSettingsRequester *)_requester requestSettings];
}

-(void)getAddressBook {
    _progress.title = NSLocalizedString(@"Getting address book", nil);
    
    MRAddressBookRequester *req = [[MRAddressBookRequester alloc] initWithRequesterDeleagate:self];
    
    self.requester = req;
    
    [req release];
    
    [(MRAddressBookRequester *)_requester requestAddressBook];
}

-(void)getLinks {
    _progress.title = NSLocalizedString(@"Getting links", nil);
    
    MRLinksRequester *req = [[MRLinksRequester alloc] initWithRequesterDeleagate:self];
    
    self.requester = req;
    
    [req release];
    
    [(MRLinksRequester *)_requester requestLinks];
}

-(void)getTemplates {
    _progress.title = NSLocalizedString(@"Getting templates", nil);
    
    MRTemplateRequester *req = [[MRTemplateRequester alloc] initWithRequesterDeleagate:self];
    
    self.requester = req;
    
    [req release];
    
    [(MRTemplateRequester *)_requester requestTemplates];
}   

-(void)getCC {
    _progress.title = NSLocalizedString(@"Getting conference calls", nil);
    
    MRConferenceCallRequester *req = [[MRConferenceCallRequester alloc] initWithRequesterDeleagate:self];
    
    self.requester = req;
    
    [req release];
    
    [(MRConferenceCallRequester *)_requester requestCCs];
}

-(void)getDocuments {
    _progress.title = NSLocalizedString(@"Getting documents", nil);
    
    MRDocumentListRequester *req = [[MRDocumentListRequester alloc] initWithRequesterDeleagate:self];
    
    self.requester = req;
    
    [req release];
    
    [(MRDocumentListRequester *)_requester requestDocuments];
}

-(void)getForms {
    _progress.title = NSLocalizedString(@"Getting forms", nil);
    
    MRFormsRequester *req = [[MRFormsRequester alloc] initWithRequesterDeleagate:self];
    
    self.requester = req;
    
    [req release];
    
    [(MRFormsRequester *)_requester requestForms];
}

-(void)unregisterCleanup {
    // TODO 
}

#pragma HttpRequesterDelegate Methods

-(void)wsCallFinished:(BOOL)wasOk withResponse:(NSNumber *)responseCode forRequest:(NSInteger)request {
   
    if(wasOk) {
        switch (request) {
            case kSettingsRequest:
                [self getAddressBook];
                break;
            case kAddressBookRequest:
                [self getLinks];
                break;
            case kLinksRequest:
                [self getTemplates];
                break;
            case kTemplateRequest:
                [self getCC];
                break;
            case kCCRequest:
                [self getForms];
                break;
            case kFormsRequest:
                [self getDocuments];
                break;
            case kDocumentsRequest:
                [self closeProgress];
                break;
            case kUnregisterRequest:
                [self closeProgress];
                
                // do unregister cleanup : delete user records, templates, urls...
                [self unregisterCleanup];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Success", nil) message:NSLocalizedString(@"Your device has been successfully unregistered", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                alert.tag = kUnregisterAlertTag;
                
                [alert show];
                [alert release];
                
                break;
            default:
                break;
        }
    } else {
        [self closeProgress];
        
        NSString *message = nil;
        switch (request) {
            case kSettingsRequest:
                message = NSLocalizedString(@"There was an error getting the settings", nil);
                break;
            case kAddressBookRequest:
                message = NSLocalizedString(@"There was an error getting the address book", nil);
                break;
            case kLinksRequest:
                message = NSLocalizedString(@"There was an error getting the links", nil);
                break;
            case kTemplateRequest:
                message = NSLocalizedString(@"There was an error getting the templates", nil);
                break;
            case kCCRequest:
                message = NSLocalizedString(@"There was an error getting the conference call list", nil);
                break;
            case kFormsRequest:
                message = NSLocalizedString(@"There was an error getting the forms", nil);
                break;
            case kDocumentsRequest:
                message = NSLocalizedString(@"There was an error getting the document list", nil);
                break;
            case kUnregisterRequest:
                message = NSLocalizedString(@"There was an error unregistering your device", nil);
                break;
            default:
                break;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        
        [alert show];
        [alert release];
    }
}

-(void)wsCallFinished:(BOOL)wasOk withResponse:(NSNumber *)responseCode{}

#pragma Progress alert view methods

-(void)showProgressAlertWithMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:NSLocalizedString(@"Please wait...", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activity.frame = CGRectMake(139.0f - 18.0f, 80.0f, 37.0f, 37.0f);
    [alert addSubview:activity];
    [activity startAnimating];
    
    self.progress = alert;
    
    [alert release];
    [activity release];
    
    [_progress show];
}

-(void)closeProgress {
    _refresh.enabled = NO;
    
    self.requester = nil;
    
    [_progress dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma UIAlertViewDelegate methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kUnregisterAlertTag) {
        exit(1);
    } else if (alertView.tag == kConfigurationAlertTag) {
        if (buttonIndex == 1) {
            NSString *password = [MRRuntimeStoreAccessor getFromRuntimeStore:kSettingsPassword];
            
            if([password isEqualToString:_configPassword.text]) {
                
                MRConfigureSettingsViewController *ctrl = [[MRConfigureSettingsViewController alloc] initWithNibName:@"MRConfigureSettingsViewController" bundle:nil];
                
                [self.navigationController pushViewController:ctrl animated:YES];
                
                [ctrl release];
            } else {
                UIAlertView *error = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"You typed an incorrect password", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                error.tag = kErrorAlertTag;
                
                [error show];
                [error release];
            }
        } 
    } else if (alertView.tag == kErrorAlertTag) {
        [self configureSettings:nil];
    }
}

@end
