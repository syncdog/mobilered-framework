//
//  MobileRED.h
//  MobileRED
//
//  Created by Dima Yarmoshuk on 9/20/15.
//  Copyright (c) 2015 SyncDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RuntimePersistenceHelper.h"
#import <UIKit/UIKit.h>

@interface MobileRED : NSObject

@property (nonatomic, retain) UINavigationController *navController;
@property (nonatomic, retain) RuntimePersistenceHelper *helper;
@property (nonatomic, retain) NSMutableDictionary *runtimeStore;

+ (instancetype)sharedManager;
- (UIViewController *)mobileRedMainViewController;

- (void)applicationDidEnterBackground;
- (void)applicationWillEnterForeground;
- (void)applicationDidBecomeActive;
- (void)applicationWillTerminate;
- (void)test;
@end
